const loadStylingConfig = (stylingConfig) => ({
  type: 'LOAD_STYLING_CONFIG',
  meta: stylingConfig.method,
  payload: stylingConfig,
});

export default loadStylingConfig;
