export const addHistoryRecord = (
  tags, datetime, hash, baseState, styleState, synthesizedImage, stylingConfig,
) => ({
  type: 'ADD_HISTORY_RECORD',
  payload: {
    tags,
    datetime,
    hash,
    baseState,
    styleState,
    synthesizedImage,
    stylingConfig,
  },
});

export const removeHistoryRecord = (hash) => ({
  type: 'REMOVE_HISTORY_RECORD',
  payload: { hash },
});

export const editHistoryRecord = (hash, editedRecord) => ({
  type: 'EDIT_HISTORY_RECORD',
  payload: { hash, editedRecord },
});

export const loadHistoryRecords = (history) => ({
  type: 'LOAD_HISTORY',
  payload: history,
});
