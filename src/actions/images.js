import {
  defaultInputStage,
  defaultSizeStage,
  defaultCropStage,
  defaultColourStage,
  defaultOutputStage,
} from './defaultStageConfigs';

export const loadContainer = (type, container) => ({
  type: 'CONTAINER',
  meta: { type },
  payload: container,
});

export const loadState = (type, index, state) => ({
  type: 'STATE',
  meta: { type, index },
  payload: state,
});

export const resetState = (type, index) => ({
  type: 'RESET',
  meta: { type, index },
  payload: {
    sizeStage: defaultSizeStage,
    cropStage: defaultCropStage,
    colourStage: defaultColourStage,
    outputStage: defaultOutputStage,
  },
});

export const loadMask = (
  type, index, mask, inputStage, sizeStage, cropStage, colourStage, outputStage,
) => {
  const newInputStage = { ...inputStage, mask };
  const newOutputStage = { ...outputStage, mask: colourStage.mask };
  return {
    type: 'MASK',
    meta: { type, index },
    payload: {
      inputStage: newInputStage,
      sizeStage,
      cropStage,
      colourStage,
      outputStage: newOutputStage,
    },
  };
};

export const loadImage = (type, index, image, hash, width, height) => {
  const inputStage = {
    ...defaultInputStage,
    image,
    hash,
    width,
    height,
  };
  const sizeStage = {
    ...defaultSizeStage,
    image,
    width,
    height,
  };
  const cropStage = { ...defaultCropStage, image };
  const colourStage = { ...defaultColourStage, image };
  const outputStage = { ...defaultOutputStage, image };
  return {
    type: 'INPUT',
    meta: { type, index },
    payload: {
      inputStage,
      sizeStage,
      cropStage,
      colourStage,
      outputStage,
    },
  };
};

export const removeImage = (type, index) => ({
  type: 'REMOVE',
  meta: { type, index },
});

export const removeMask = (type, index) => ({
  type: 'UNMASK',
  meta: { type, index },
});

export const resizeImage = (type, index, sizeStage, cropStage, colourStage) => {
  const outputStage = {
    stage: 'output',
    image: colourStage.image,
    mask: colourStage.mask,
  };
  return {
    type: 'SIZE',
    meta: { type, index },
    payload: {
      sizeStage,
      cropStage,
      colourStage,
      outputStage,
    },
  };
};

export const cropImage = (type, index, cropStage, colourStage) => {
  const outputStage = {
    stage: 'output',
    image: colourStage.image,
    mask: colourStage.mask,
  };
  return {
    type: 'CROP',
    meta: { type, index },
    payload: {
      cropStage,
      colourStage,
      outputStage,
    },
  };
};

export const recolourImage = (type, index, colourStage) => {
  const outputStage = {
    stage: 'output',
    image: colourStage.image,
    mask: colourStage.mask,
  };
  return {
    type: 'COLOUR',
    meta: { type, index },
    payload: {
      colourStage,
      outputStage,
    },
  };
};

export const changeIndex = (type, index) => ({
  type: 'INDEX',
  meta: { type, index },
});
