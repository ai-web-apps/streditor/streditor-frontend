export const defaultInputStage = {
  stage: 'input',
  mask: null,
  hash: null,
  width: null,
  height: null,
};

export const defaultSizeStage = {
  stage: 'size',
  mask: null,
  width: null,
  height: null,
};

export const defaultCropStage = {
  stage: 'crop',
  mask: null,
  x: 0.0,
  y: 0.0,
  width: 1.0,
  height: 1.0,
};

export const defaultColourStage = {
  stage: 'colour',
  mask: null,
  brightness: 50,
  contrast: 50,
  red: 50,
  green: 50,
  blue: 50,
};

export const defaultOutputStage = {
  stage: 'output',
  mask: null,
};
