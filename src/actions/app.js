const changeAppState = (newState) => {
  switch (newState) {
    case 'processing':
      return { type: 'APP_PROCESSING' };
    default:
      return { type: 'APP_IDLE' };
  }
};

export default changeAppState;
