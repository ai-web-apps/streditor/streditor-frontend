import {
  shape, number, string, oneOf, instanceOf, objectOf,
} from 'prop-types';

export const imageTypeProp = oneOf(['base', 'style', 'synthesized']);

export const imageIndexProp = number;

export const imageStateProp = shape({
  inputStage: shape({ stage: oneOf(['input']), image: string }),
  sizeStage: shape({
    stage: oneOf(['size']),
    image: string,
    width: number,
    height: number,
  }),
  cropStage: shape({
    stage: oneOf(['crop']),
    image: string,
    x: number,
    y: number,
    width: number,
    height: number,
  }),
  colourStage: shape({
    stage: oneOf(['colour']),
    image: string,
    brightness: number,
    contrast: number,
    red: number,
    green: number,
    blue: number,
  }),
  outputStage: shape({ stage: oneOf(['output']), image: string }),
});

export const imageContainerProp = objectOf(imageStateProp);

export const stylingConfigProp = shape({ method: string });

export const historyRecordProp = shape({
  tags: string,
  datetime: instanceOf(Date),
  hash: string,
  baseImage: imageStateProp,
  styleImage: imageStateProp,
  synthesizedImage: string,
  stylingConfig: stylingConfigProp,
});

export const vggLayersProp = shape({
  block1_conv1: number,
  block1_conv2: number,
  block2_conv1: number,
  block2_conv2: number,
  block3_conv1: number,
  block3_conv2: number,
  block3_conv3: number,
  block3_conv4: number,
  block4_conv1: number,
  block4_conv2: number,
  block4_conv3: number,
  block4_conv4: number,
  block5_conv1: number,
  block5_conv2: number,
  block5_conv3: number,
  block5_conv4: number,
});
