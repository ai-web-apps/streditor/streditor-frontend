import { IMAGE_BOUNDS } from './constraints';
import { loadImage, loadMask } from '../actions/images';
import { preprocessImage, validateMasks } from './requests';

export const computeCanvasDimensions = ({
  imageWidth,
  imageHeight,
  viewerWidth,
  viewerHeight,
  marginPercentage = 0.025,
}) => {
  const widthRatio = imageWidth / viewerWidth;
  const heightRatio = imageHeight / viewerHeight;
  const maxRatio = Math.max(widthRatio, heightRatio) + marginPercentage;
  const newWidth = Math.round(imageWidth / maxRatio);
  const newHeight = Math.round(imageHeight / maxRatio);
  return { newWidth, newHeight };
};

export const getIndexFromHash = (container, hash) => {
  let i = 0;
  while (i < Object.keys(container).length) {
    if (container[i].inputStage.hash === hash) {
      return i;
    }
    i += 1;
  }
  return i;
};

export const loadExistingImage = (type, index, image, hash, dispatch) => {
  const img = new Image();

  img.onload = () => {
    dispatch(loadImage(type, index, image, hash, img.width, img.height));
  };
  img.src = image;
};

const getImageResizingRatio = (img) => {
  if ((img.width < IMAGE_BOUNDS.minWidth && img.height > IMAGE_BOUNDS.maxHeight)
  || (img.width > IMAGE_BOUNDS.maxWidth && img.height < IMAGE_BOUNDS.minHeight)) {
    return -1;
  }

  if (img.width < IMAGE_BOUNDS.minWidth || img.height < IMAGE_BOUNDS.minHeight) {
    const widthRatio = IMAGE_BOUNDS.minWidth / img.width;
    const heightRatio = IMAGE_BOUNDS.minHeight / img.height;
    return Math.max(widthRatio, heightRatio);
  }

  if (img.width > IMAGE_BOUNDS.maxWidth || img.height > IMAGE_BOUNDS.maxHeight) {
    const widthRatio = IMAGE_BOUNDS.maxWidth / img.width;
    const heightRatio = IMAGE_BOUNDS.maxHeight / img.height;
    return Math.min(widthRatio, heightRatio);
  }

  return 0;
};

export const uploadNewImage = (type, index, image, dispatch) => {
  const img = new Image();
  let executions = {
    0: {
      stage: 'input',
    },
  };

  img.onload = () => {
    const resizingRatio = getImageResizingRatio(img);

    switch (resizingRatio) {
      case -1:
        console.error('Cannot upload image because of its shape');
        break;

      case 0:
        preprocessImage(img.src, null, executions).then((response) => {
          const inputStage = response.data.completions[0];
          dispatch(loadImage(type, index, img.src, inputStage.hash, img.width, img.height));
        }).catch((error) => console.error(error));
        break;

      default:
        executions = {
          ...executions,
          1: {
            stage: 'size',
            width: Math.round(img.width * resizingRatio),
            height: Math.round(img.height * resizingRatio),
          },
        };
        preprocessImage(img.src, null, executions).then((response) => {
          const inputStage = response.data.completions[0];
          const sizeStage = response.data.completions[1];
          dispatch(
            loadImage(
              type, index, sizeStage.image, inputStage.hash, sizeStage.width, sizeStage.height,
            ),
          );
          console.warn(
            `Image dimensions out of allowed bounds. Resizing to ${sizeStage.width} x ${sizeStage.height}.`,
          );
        }).catch((error) => console.error(error));
    }
  };
  img.src = image;
};

export const uploadNewMask = (type, index, imageState, mask, dispatch) => {
  const msk = new Image();
  const imageWidth = imageState.inputStage.width;
  const imageHeight = imageState.inputStage.height;

  msk.onload = () => {
    const resizingRatio = getImageResizingRatio(msk);

    switch (resizingRatio) {
      case -1:
        console.error('Cannot upload mask because of its shape');
        break;

      case 0:
        if (msk.width === imageWidth && msk.height === imageHeight) {
          const executions = {
            0: imageState.sizeStage,
            1: imageState.cropStage,
            2: imageState.colourStage,
          };
          preprocessImage(imageState.inputStage.image, mask, executions).then((response) => {
            dispatch(loadMask(
              type,
              index,
              mask,
              imageState.inputStage,
              response.data.completions[0],
              response.data.completions[1],
              response.data.completions[2],
              imageState.outputStage,
            ));
          }).catch((error) => {
            console.error(error);
          });
        } else {
          console.error(
            `Mask shape (${msk.width}, ${msk.height}) does not `
            + `match image shape (${imageWidth}, ${imageHeight}).`,
          );
        }
        break;

      default:
      {
        const maskResizeExecutions = {
          1: {
            stage: 'size',
            width: Math.round(msk.width * resizingRatio),
            height: Math.round(msk.height * resizingRatio),
          },
        };
        preprocessImage(null, msk.src, maskResizeExecutions).then((maskResizeResponse) => {
          const maskResizeStage = maskResizeResponse.data.completions[1];
          if (maskResizeStage.width === imageWidth && maskResizeStage.height === imageHeight) {
            const executions = {
              0: imageState.sizeStage,
              1: imageState.cropStage,
              2: imageState.colourStage,
            };
            preprocessImage(
              imageState.inputStage.image, maskResizeStage.mask, executions,
            ).then((response) => {
              dispatch(loadMask(
                type,
                index,
                maskResizeStage.mask,
                imageState.inputStage,
                response.data.completions[0],
                response.data.completions[1],
                response.data.completions[2],
                imageState.outputStage,
              ));
            }).catch((error) => {
              console.error(error);
            });
          } else {
            console.error(
              `Mask shape (${msk.width}, ${msk.height}) does not `
              + `match image shape (${imageWidth}, ${imageHeight}).`,
            );
          }
        }).catch((error) => console.error(error));
      }
    }
  };
  msk.src = mask;
};

export const masksValidation = (baseMask, styleMask, setMasksValid) => {
  validateMasks(baseMask, styleMask).then((response) => {
    setMasksValid(response.data.valid);
  }).catch((error) => console.error(error));
};

export const getRunDisabledReason = (baseState, styleState, masksValid, appState) => {
  if (!baseState) {
    return 'Base image is missing';
  }
  if (!styleState) {
    return 'Style image is missing';
  }
  if (!masksValid) {
    return 'Masks\' labels are not matching';
  }
  if (appState !== 'idle') {
    return 'App is busy performing a style transfer operation';
  }
  return 'OK';
};
