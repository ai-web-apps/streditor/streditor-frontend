export const capitalizeFirstLetter = (string) => string.charAt(0).toUpperCase() + string.slice(1);

export const datetimeToString = (dt) => {
  const zeroPad = (x) => (x < 10 ? `0${x}` : `${x}`);

  const date = `${zeroPad(dt.getDate())}-${zeroPad(dt.getMonth() + 1)}-${zeroPad(dt.getFullYear() % 100)}`;
  const time = `${zeroPad(dt.getHours())}:${zeroPad(dt.getMinutes())}`;

  return `${date}@${time}`;
};

export const getBase64Extension = (image) => image.split('image/').pop().split(';')[0];

export const parseJsonBase64 = (data) => {
  const parts = data.split(',');
  console.assert(parts[0] === 'data:application/json;base64');
  return parts[1];
};

export const imageHashToId = (hash) => hash.substring(0, 12);
