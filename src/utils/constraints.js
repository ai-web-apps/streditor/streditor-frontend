export const IMAGE_BOUNDS = {
  minWidth: 64,
  maxWidth: 2048,
  minHeight: 64,
  maxHeight: 1536,
};

export const MAX_FILES = 10;

export const MAX_SCALE = 10;

export const IMAGE_TYPES = 'image/jpeg, image/png';

export const MASK_TYPES = 'image/png';
