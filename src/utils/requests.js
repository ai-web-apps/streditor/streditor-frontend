import axios from 'axios';

export const checkBackendConnection = async () => {
  const url = `${process.env.REACT_APP_FLASK_HOST}/stram/`;
  console.log(`Pinging ${url}:`);
  await axios.get(url).then((response) => console.log(response));
};

export const preprocessImage = async (image, mask, executions) => {
  const url = `${process.env.REACT_APP_FLASK_HOST}/preprocessing/execute/`;

  const bodyFormData = new FormData();
  bodyFormData.append('executions', JSON.stringify(executions));
  if (image !== null) {
    bodyFormData.append('image', image);
  }
  if (mask !== null) {
    bodyFormData.append('mask', mask);
  }

  const result = await axios({
    method: 'post',
    url,
    data: bodyFormData,
    headers: { 'Content-Type': 'multipart/form-data' },
  })
    .then((response) => response)
    .catch((error) => {
      console.error(error.message);
    });
  return result;
};

export const colourMask = async (mask) => {
  const url = `${process.env.REACT_APP_FLASK_HOST}/masking/colour/`;

  const bodyFormData = new FormData();
  bodyFormData.append('mask', mask);

  const result = await axios({
    method: 'post',
    url,
    data: bodyFormData,
    headers: { 'Content-Type': 'multipart/form-data' },
  })
    .then((response) => response)
    .catch((error) => {
      console.error(error.message);
    });
  return result;
};

export const validateMasks = async (baseMask, styleMask) => {
  const url = `${process.env.REACT_APP_FLASK_HOST}/masking/validate/`;

  const bodyFormData = new FormData();
  if (baseMask !== null) {
    bodyFormData.append('baseMask', baseMask);
  }
  if (styleMask !== null) {
    bodyFormData.append('styleMask', styleMask);
  }

  const result = await axios({
    method: 'post',
    url,
    data: bodyFormData,
    headers: { 'Content-Type': 'multipart/form-data' },
  })
    .then((response) => response)
    .catch((error) => {
      console.error(error.message);
    });
  return result;
};

export const orderStyleTransfer = async (
  baseImage, styleImage, baseMask, styleMask, stylingConfig,
) => {
  const url = `${process.env.REACT_APP_FLASK_HOST}/stram/order/`;

  const bodyFormData = new FormData();
  bodyFormData.append('baseImage', baseImage);
  bodyFormData.append('styleImage', styleImage);
  bodyFormData.append('stylingConfig', JSON.stringify(stylingConfig));

  if (baseMask !== null) {
    bodyFormData.append('baseMask', baseMask);
  }
  if (styleMask !== null) {
    bodyFormData.append('styleMask', styleMask);
  }

  const result = await axios({
    method: 'post',
    url,
    data: bodyFormData,
    headers: { 'Content-Type': 'multipart/form-data' },
  })
    .then((response) => response)
    .catch((error) => {
      console.error(error.message);
    });
  return result;
};

export const exportFigure = async (baseImage, styleImage, synthesizedImage) => {
  const url = `${process.env.REACT_APP_FLASK_HOST}/stram/export/`;

  const bodyFormData = new FormData();
  bodyFormData.append('baseImage', baseImage);
  bodyFormData.append('styleImage', styleImage);
  bodyFormData.append('synthesizedImage', synthesizedImage);

  const result = await axios({
    method: 'post',
    url,
    data: bodyFormData,
    headers: { 'Content-Type': 'multipart/form-data' },
  })
    .then((response) => response)
    .catch((error) => {
      console.error(error.message);
    });
  return result;
};
