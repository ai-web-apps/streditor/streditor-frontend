import React, { useSelector } from 'react-redux';
import EmptyViewer from './EmptyViewer';
import ImageViewer from './ImageViewer';
import ImageSpinner from './ImageSpinner';
import ImageUploader from './ImageUploader';
import HistoryViewer from './HistoryViewer';
import '../style/workspace.scss';

function Workspace() {
  const appState = useSelector((state) => state.app);
  const history = useSelector((state) => state.history);
  const baseIndex = useSelector((state) => state.baseIndex);
  const baseContainer = useSelector((state) => state.baseContainer);
  const styleIndex = useSelector((state) => state.styleIndex);
  const styleContainer = useSelector((state) => state.styleContainer);
  const synthesizedIndex = useSelector((state) => state.synthesizedIndex);
  const synthesizedContainer = useSelector((state) => state.synthesizedContainer);

  const renderSynthesized = () => {
    if (appState === 'processing') {
      return <ImageSpinner title="Synthesized Image" />;
    }
    if (synthesizedIndex === null
      || !Object.keys(synthesizedContainer).length
      || !(synthesizedIndex in synthesizedContainer)) {
      return <EmptyViewer title="Synthesized Image" />;
    }
    return (
      <ImageViewer
        type="synthesized"
        index={synthesizedIndex}
        container={synthesizedContainer}
      />
    );
  };

  return (
    <div className="workspace-container">
      <div className="top-container">
        {baseIndex === null
          || !Object.keys(baseContainer).length
          || !(baseIndex in baseContainer)
          ? <ImageUploader type="base" />
          : <ImageViewer type="base" index={baseIndex} container={baseContainer} />}
        {renderSynthesized()}
      </div>
      <div className="bottom-container">
        {styleIndex === null
          || !Object.keys(styleContainer).length
          || !(styleIndex in styleContainer)
          ? <ImageUploader type="style" />
          : <ImageViewer type="style" index={styleIndex} container={styleContainer} />}
        <HistoryViewer title="History" history={history} />
      </div>
    </div>
  );
}

export default Workspace;
