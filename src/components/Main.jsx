import React from 'react';
import Sidebar from './Sidebar';
import Workspace from './Workspace';
import '../style/main.scss';

function Main() {
  return (
    <div className="main">
      <Sidebar />
      <Workspace />
    </div>
  );
}

export default Main;
