import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Table from 'react-bootstrap/Table';
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import { TiDelete } from 'react-icons/ti';
import { RiFilterOffLine } from 'react-icons/ri';
import { historyRecordProp } from '../utils/props';
import { datetimeToString, imageHashToId } from '../utils/strings';
import { removeHistoryRecord, editHistoryRecord } from '../actions/history';
import HistoryRecordModal from './modals/HistoryRecordModal';
import '../style/history-viewer.scss';
import '../style/workspace-quarter.scss';

function HistoryViewer({ title, history }) {
  const dispatch = useDispatch();
  const [filterId, setFilterId] = useState('');
  const [filterTags, setFilterTags] = useState('');
  const [filterDatetime, setFilterDatetime] = useState('');
  const [tagsEditing, setTagsEditing] = useState('');
  const [tagsBeingEdited, setTagsBeingEdited] = useState(null);
  const [recordModalIsOpen, setRecordModalIsOpen] = useState(false);
  const [recordModalRecord, setRecordModalRecord] = useState(null);

  const removeRecord = (hash) => dispatch(removeHistoryRecord(hash));

  const openRecordModal = (record) => {
    setRecordModalRecord(record);
    setRecordModalIsOpen(true);
  };

  const resetFilters = () => {
    setFilterId('');
    setFilterTags('');
    setFilterDatetime('');
  };

  const isMatchingTags = (recordTags) => {
    const filterList = filterTags.split(' ');
    const recordList = recordTags.split(' ');
    return filterList.every((tag) => tag === '' || recordList.includes(tag));
  };

  const historyRecords = history
    .filter((record) => imageHashToId(record.hash).includes(filterId)
      && datetimeToString(record.datetime).includes(filterDatetime)
      && isMatchingTags(record.tags))
    .sort((a, b) => ((a.datetime < b.datetime) ? 1 : -1))
    .map((record) => (
      <tr key={record.hash}>
        <td>
          <div className="image-td">
            <Image
              src={record.synthesizedImage}
              onClick={() => openRecordModal(record)}
              rounded
              fluid
            />
          </div>
        </td>
        <td>
          {tagsBeingEdited === record.hash
            ? (
              <form
                className="tags-edit"
                onSubmit={(event) => {
                  if (event) {
                    event.preventDefault();
                    const editedRecord = { ...record, tags: tagsEditing };
                    dispatch(editHistoryRecord(record.hash, editedRecord));
                    setTagsBeingEdited(null);
                  }
                }}
              >
                <input
                  value={tagsEditing}
                  onChange={(event) => { setTagsEditing(event.target.value); }}
                />
                <input type="submit" hidden />
              </form>
            )
            : (
              <div
                role="button"
                tabIndex={0}
                className="tags-static"
                onClick={() => {
                  setTagsEditing(record.tags);
                  setTagsBeingEdited(record.hash);
                }}
                onKeyDown={() => {
                  setTagsEditing(record.tags);
                  setTagsBeingEdited(record.hash);
                }}
              >
                {record.tags}
              </div>
            )}
        </td>
        <td>{imageHashToId(record.hash)}</td>
        <td>{datetimeToString(record.datetime)}</td>
        <td>
          <Button variant="danger" onClick={() => removeRecord(record.hash)}>
            <TiDelete size="24" />
          </Button>
        </td>
      </tr>
    ));

  return (
    <div className="workspace-quarter">
      <div className="title">
        {title}
      </div>
      <div className="history-panel">
        <Table striped bordered responsive hover variant="dark">
          <thead>
            <tr>
              <th width="15%">Image</th>
              <th width="30%">Tags</th>
              <th width="22%">Id</th>
              <th width="28%">Date and Time</th>
              <th width="5%">Remove</th>
            </tr>
            <tr>
              <td>Filters</td>
              <td>
                <input
                  className="filter-input"
                  value={filterTags}
                  onChange={(event) => setFilterTags(event.target.value)}
                />
              </td>
              <td>
                <input
                  className="filter-input"
                  value={filterId}
                  onChange={(event) => setFilterId(event.target.value)}
                />
              </td>
              <td>
                <input
                  className="filter-input"
                  value={filterDatetime}
                  onChange={(event) => setFilterDatetime(event.target.value)}
                />
              </td>
              <td>
                <Button variant="secondary">
                  <RiFilterOffLine size="24" onClick={resetFilters} />
                </Button>
              </td>
            </tr>
          </thead>
          <tbody>
            {historyRecords}
          </tbody>
        </Table>
      </div>
      <HistoryRecordModal
        isOpen={recordModalIsOpen}
        setIsOpen={setRecordModalIsOpen}
        record={recordModalRecord}
      />
    </div>
  );
}

HistoryViewer.propTypes = {
  title: PropTypes.string.isRequired,
  history: PropTypes.arrayOf(historyRecordProp).isRequired,
};

export default HistoryViewer;
