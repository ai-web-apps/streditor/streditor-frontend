import React, { useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useDropzone } from 'react-dropzone';
import Button from 'react-bootstrap/Button';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import { RiFolderDownloadLine, RiFolderUploadLine } from 'react-icons/ri';
import { parseJsonBase64 } from '../utils/strings';
import { loadContainer, changeIndex } from '../actions/images';
import { loadHistoryRecords } from '../actions/history';
import '../style/header.scss';
import logo from '../assets/logo_white_transparent.png';

const BUTTON_SIZE = '24';

function Header() {
  const dispatch = useDispatch();
  const [stateName, setStateName] = useState('');
  const appState = useSelector((state) => state.app);
  const history = useSelector((state) => state.history);
  const baseIndex = useSelector((state) => state.baseIndex);
  const baseContainer = useSelector((state) => state.baseContainer);
  const styleIndex = useSelector((state) => state.styleIndex);
  const styleContainer = useSelector((state) => state.styleContainer);
  const synthesizedIndex = useSelector((state) => state.synthesizedIndex);
  const synthesizedContainer = useSelector((state) => state.synthesizedContainer);

  const packageState = () => ({
    history,
    baseIndex,
    baseContainer,
    styleIndex,
    styleContainer,
    synthesizedIndex,
    synthesizedContainer,
  });

  const saveWorkspace = async (event) => {
    if (event) {
      event.preventDefault();

      const data = JSON.stringify(packageState(), null, 4);
      const blob = new Blob([data], { type: 'application/json' });
      const href = await URL.createObjectURL(blob);
      const link = document.createElement('a');

      link.href = href;
      link.download = stateName;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  };

  const loadWorkspace = useCallback((files) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      const jsonBase64 = parseJsonBase64(event.target.result);
      const state = JSON.parse(window.atob(jsonBase64));

      dispatch(changeIndex('base', state.baseIndex));
      dispatch(loadContainer('base', state.baseContainer));
      dispatch(changeIndex('style', state.styleIndex));
      dispatch(loadContainer('style', state.styleContainer));
      dispatch(changeIndex('synthesized', state.synthesizedIndex));
      dispatch(loadContainer('synthesized', state.synthesizedContainer));
      dispatch(loadHistoryRecords(state.history));
    };
    reader.readAsDataURL(files[0]);
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: loadWorkspace,
    accept: 'application/json',
    maxFiles: 1,
  });

  const popover = (
    <Popover>
      <Popover.Header>Insert state name</Popover.Header>
      <Popover.Body>
        <form onSubmit={saveWorkspace} className="popover-form">
          <input
            className="input"
            required
            maxLength="20"
            value={stateName}
            onChange={(event) => setStateName(event.target.value)}
          />
          <Button
            variant="success"
            size="sm"
            type="submit"
            disabled={appState !== 'idle'}
          >
            Save
          </Button>
        </form>
      </Popover.Body>
    </Popover>
  );

  return (
    <header className="header">
      <div className="header-side-left">
        <img src={logo} className="header-logo" alt="Streditor" />
      </div>
      <div className="header-side-right">
        <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
          <Button variant="success" disabled={appState !== 'idle'}>
            <RiFolderDownloadLine size={BUTTON_SIZE} />
          </Button>
        </OverlayTrigger>
        <Button {...getRootProps({ variant: 'info', disabled: appState !== 'idle' })}>
          <input {...getInputProps()} />
          <RiFolderUploadLine size={BUTTON_SIZE} />
        </Button>
      </div>
    </header>
  );
}

export default Header;
