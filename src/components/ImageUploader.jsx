import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useDropzone } from 'react-dropzone';
import { ImUpload } from 'react-icons/im';
import { changeIndex } from '../actions/images';
import { imageTypeProp } from '../utils/props';
import { uploadNewImage } from '../utils/images';
import { MAX_FILES, IMAGE_TYPES } from '../utils/constraints';
import { capitalizeFirstLetter } from '../utils/strings';
import '../style/workspace-quarter.scss';
import '../style/image-uploader.scss';

function ImageUploader({ type }) {
  const dispatch = useDispatch();
  const onDrop = useCallback(
    (files) => {
      for (let index = 0; index < files.length; index += 1) {
        const reader = new FileReader();

        reader.onload = (event) => {
          uploadNewImage(type, index, event.target.result, dispatch);
          dispatch(changeIndex(type, index));
        };
        reader.readAsDataURL(files[index]);
      }
    },
    [type],
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: IMAGE_TYPES,
    maxFiles: MAX_FILES,
  });

  return (
    <div className="workspace-quarter">
      <div className="title">
        {capitalizeFirstLetter(type)}
        {' '}
        Image
      </div>
      <div {...getRootProps({ className: 'image-uploader-area' })}>
        <input {...getInputProps()} />
        <p className="upload-message">
          {isDragActive ? `Drop ${capitalizeFirstLetter(type)} Image`
            : <ImUpload size="60" />}
        </p>
      </div>
    </div>
  );
}

ImageUploader.propTypes = {
  type: imageTypeProp.isRequired,
};

export default ImageUploader;
