import React from 'react';
import PropTypes from 'prop-types';
import Slider, { createSliderWithTooltip } from 'rc-slider';
import { capitalizeFirstLetter } from '../../utils/strings';
import 'rc-slider/assets/index.css';
import '../../style/input-elements/slider.scss';

const SliderWithTooltip = createSliderWithTooltip(Slider);

function SliderWrap(props) {
  const {
    name, min, max, step, value, onChange, onAfterChange,
  } = props;

  return (
    <div className="slider-div">
      <div className="slider-name">{capitalizeFirstLetter(name)}</div>
      <SliderWithTooltip
        className="slider-slider"
        min={min}
        max={max}
        step={step}
        value={value}
        onChange={(newValue) => onChange(name, newValue)}
        onAfterChange={onAfterChange}
      />
    </div>
  );
}

SliderWrap.propTypes = {
  name: PropTypes.string.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  onAfterChange: PropTypes.func.isRequired,
};

export default SliderWrap;
