import React, { useRef, useEffect, useState } from 'react';
import InspectModal from './modals/InspectModal';
import { computeCanvasDimensions } from '../utils/images';
import { imageIndexProp, imageStateProp } from '../utils/props';
import '../style/image-viewer.scss';

function ImageViewerCentralPanel({ index, imageState }) {
  const canvasRef = useRef(null);
  const canvasDivRef = useRef(null);
  const [inspectModalIsOpen, setInspectModalIsOpen] = useState(false);

  useEffect(() => {
    const canvasDiv = canvasDivRef.current;
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');
    const image = new Image();

    image.onload = () => {
      const { newWidth, newHeight } = computeCanvasDimensions({
        imageWidth: image.width,
        imageHeight: image.height,
        viewerWidth: canvasDiv.offsetWidth,
        viewerHeight: canvasDiv.offsetHeight,
      });
      canvas.width = newWidth;
      canvas.height = newHeight;
      canvas.style.width = `${newWidth}px`;
      canvas.style.height = `${newHeight}px`;
      ctx.drawImage(image, 0, 0, newWidth, newHeight);
    };
    image.src = imageState.outputStage.image;
  }, [canvasRef, canvasDivRef, imageState, index]);

  return (
    <div className="canvas-div" ref={canvasDivRef}>
      <canvas
        className="canvas-central"
        ref={canvasRef}
        onClick={() => setInspectModalIsOpen(true)}
      />
      <InspectModal
        isOpen={inspectModalIsOpen}
        setIsOpen={setInspectModalIsOpen}
        image={imageState.outputStage.image}
      />
    </div>

  );
}

ImageViewerCentralPanel.propTypes = {
  index: imageIndexProp.isRequired,
  imageState: imageStateProp.isRequired,
};

export default ImageViewerCentralPanel;
