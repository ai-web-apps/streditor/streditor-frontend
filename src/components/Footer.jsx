import React from 'react';
import '../style/footer.scss';

function Footer() {
  return (
    <footer className="footer">
      © 2021 Emil Barbuta. All rights reserved.
    </footer>
  );
}

export default Footer;
