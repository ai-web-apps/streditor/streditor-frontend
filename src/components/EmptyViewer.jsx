import React from 'react';
import PropTypes from 'prop-types';
import { FaRegFileImage } from 'react-icons/fa';
import '../style/workspace-quarter.scss';

function EmptyViewer({ title }) {
  return (
    <div className="workspace-quarter">
      <div className="title">
        {title}
      </div>
      <div className="misc-icon">
        <FaRegFileImage size="60" />
      </div>
    </div>
  );
}

EmptyViewer.propTypes = {
  title: PropTypes.string.isRequired,
};

export default EmptyViewer;
