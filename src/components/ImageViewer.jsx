import React from 'react';
import { useDispatch } from 'react-redux';
import Carousel from 'react-bootstrap/Carousel';
import ImageViewerLeftPanel from './ImageViewerLeftPanel';
import ImageViewerRightPanel from './ImageViewerRightPanel';
import ImageViewerCentralPanel from './ImageViewerCentralPanel';
import { changeIndex } from '../actions/images';
import { capitalizeFirstLetter } from '../utils/strings';
import { imageTypeProp, imageIndexProp, imageContainerProp } from '../utils/props';
import '../style/workspace-quarter.scss';
import '../style/image-viewer.scss';

function ImageViewer({ type, index, container }) {
  const dispatch = useDispatch();

  const carouselItems = Object.keys(container).sort()
    .map((keyString) => {
      const key = parseInt(keyString, 10);
      return (
        <Carousel.Item key={key}>
          <ImageViewerCentralPanel index={index} imageState={container[key]} />
        </Carousel.Item>
      );
    });

  const handleSelect = (selectedIndex, event) => {
    event.preventDefault();
    dispatch(changeIndex(type, selectedIndex));
  };

  return (
    <div className="workspace-quarter">
      <div className="title">
        {capitalizeFirstLetter(type)}
        {' '}
        Image
      </div>
      <div className="image-viewer-content">
        <ImageViewerLeftPanel type={type} index={index} container={container} />
        <Carousel interval={null} activeIndex={index} onSelect={handleSelect}>
          {carouselItems}
        </Carousel>
        <ImageViewerRightPanel type={type} index={index} container={container} />
      </div>
    </div>
  );
}

ImageViewer.propTypes = {
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
  container: imageContainerProp.isRequired,
};

export default ImageViewer;
