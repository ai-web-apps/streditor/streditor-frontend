import React from 'react';
import PropTypes from 'prop-types';
import { GridLoader } from 'react-spinners';
import '../style/workspace-quarter.scss';

function ImageSpinner({ title }) {
  return (
    <div className="workspace-quarter">
      <div className="title">
        {title}
      </div>
      <div className="misc">
        <GridLoader loading color="#ffffff" size={36} />
      </div>
    </div>
  );
}

ImageSpinner.propTypes = {
  title: PropTypes.string.isRequired,
};

export default ImageSpinner;
