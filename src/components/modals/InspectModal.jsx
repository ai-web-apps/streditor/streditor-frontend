import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import PinchZoomPan from 'react-responsive-pinch-zoom-pan';
import { MAX_SCALE } from '../../utils/constraints';
import '../../style/modals/editing-modal.scss';

function InspectModal({ isOpen, setIsOpen, image }) {
  const onHide = () => setIsOpen(false);

  return (
    <Modal
      fullscreen
      show={isOpen}
      onHide={onHide}
      className="editing-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title>
          Inspect Image
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="modal-image">
          <PinchZoomPan position="center" maxScale={MAX_SCALE}>
            <img src={image} alt="Missing Resource" />
          </PinchZoomPan>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

InspectModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  setIsOpen: PropTypes.func.isRequired,
  image: PropTypes.string.isRequired,
};

export default InspectModal;
