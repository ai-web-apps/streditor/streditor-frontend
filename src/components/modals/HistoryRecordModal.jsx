import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';
import JSONPretty from 'react-json-pretty';
import loadStylingConfig from '../../actions/configs';
import { loadState, changeIndex } from '../../actions/images';
import { exportFigure } from '../../utils/requests';
import { historyRecordProp } from '../../utils/props';
import { imageHashToId, getBase64Extension } from '../../utils/strings';
import { loadExistingImage, getIndexFromHash } from '../../utils/images';
import '../../style/modals/history-record-modal.scss';
import 'react-json-pretty/themes/monikai.css';

function ImageFrame({
  title, image, isSelected, setIsSelected,
}) {
  return (
    <div
      role="button"
      tabIndex={0}
      className={isSelected ? 'image-frame-selected' : 'image-frame'}
      onClick={() => setIsSelected(!isSelected)}
      onKeyDown={() => setIsSelected(!isSelected)}
    >
      <div className="title-area">
        {title}
      </div>
      <div className="image-area">
        <Image src={image} rounded fluid />
      </div>
    </div>
  );
}
function HistoryRecordModal({ isOpen, setIsOpen, record }) {
  const dispatch = useDispatch();
  const appState = useSelector((state) => state.app);
  const baseContainer = useSelector((state) => state.baseContainer);
  const styleContainer = useSelector((state) => state.styleContainer);
  const synthesizedContainer = useSelector((state) => state.synthesizedContainer);
  const [configIsSelected, setConfigIsSelected] = useState(false);
  const [baseIsSelected, setBaseIsSelected] = useState(false);
  const [styleIsSelected, setStyleIsSelected] = useState(false);
  const [synthesizedIsSelected, setSynthesizedIsSelected] = useState(false);

  const doLoading = (type, container) => {
    let index = null;
    switch (type) {
      case 'base':
        index = getIndexFromHash(container, record.baseState.inputStage.hash);
        dispatch(loadState(type, index, record.baseState));
        break;
      case 'style':
        index = getIndexFromHash(container, record.styleState.inputStage.hash);
        dispatch(loadState(type, index, record.styleState));
        break;
      default:
        index = getIndexFromHash(container, record.hash);
        loadExistingImage('synthesized', index, record.synthesizedImage, record.hash, dispatch);
    }
    dispatch(changeIndex(type, index));
  };

  const onHide = () => setIsOpen(false);

  const onEnter = () => {
    setConfigIsSelected(false);
    setBaseIsSelected(false);
    setStyleIsSelected(false);
    setSynthesizedIsSelected(false);
  };

  const onSave = () => {
    if (baseIsSelected) {
      doLoading('base', baseContainer);
    }
    if (styleIsSelected) {
      doLoading('style', styleContainer);
    }
    if (synthesizedIsSelected) {
      doLoading('synthesized', synthesizedContainer);
    }
    if (configIsSelected) {
      dispatch(loadStylingConfig(record.stylingConfig));
    }
    onHide();
  };

  const onExport = () => {
    exportFigure(
      record.baseState.outputStage.image,
      record.styleState.outputStage.image,
      record.synthesizedImage,
    ).then((response) => {
      const imageExtension = getBase64Extension(response.data.figure);
      const imageElement = document.createElement('a');

      imageElement.href = response.data.figure;
      imageElement.download = `figure_${record.hash}.${imageExtension}`;
      imageElement.click();
    });
  };

  return (
    record && (
      <Modal
        size="lg"
        show={isOpen}
        onHide={onHide}
        onEnter={onEnter}
        className="history-record-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title>
            {`Record [${imageHashToId(record.hash)}]`}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            role="button"
            tabIndex={0}
            className={configIsSelected ? 'modal-config-selected' : 'modal-config'}
            onClick={() => setConfigIsSelected(!configIsSelected)}
            onKeyDown={() => setConfigIsSelected(!configIsSelected)}
          >
            <JSONPretty className="json-pretty" data={record.stylingConfig} space="4" />
          </div>
          <div className="modal-images">
            <ImageFrame
              title="Synthesized Image"
              image={record.synthesizedImage}
              isSelected={synthesizedIsSelected}
              setIsSelected={setSynthesizedIsSelected}
            />
            <div className="input-images">
              <ImageFrame
                title="Base Image"
                image={record.baseState.outputStage.image}
                isSelected={baseIsSelected}
                setIsSelected={setBaseIsSelected}
              />
              <ImageFrame
                title="Style Image"
                image={record.styleState.outputStage.image}
                isSelected={styleIsSelected}
                setIsSelected={setStyleIsSelected}
              />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onHide}>
            Close
          </Button>
          <Button variant="primary" onClick={onExport}>
            Export figure
          </Button>
          <Button
            variant="success"
            onClick={onSave}
            disabled={
              appState !== 'idle'
              || !(configIsSelected || baseIsSelected || styleIsSelected || synthesizedIsSelected)
            }
          >
            Load into workspace
          </Button>
        </Modal.Footer>
      </Modal>
    )
  );
}

ImageFrame.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  isSelected: PropTypes.bool.isRequired,
  setIsSelected: PropTypes.func.isRequired,
};

HistoryRecordModal.defaultProps = {
  record: () => null,
};

HistoryRecordModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  setIsOpen: PropTypes.func.isRequired,
  record: historyRecordProp,
};

export default HistoryRecordModal;
