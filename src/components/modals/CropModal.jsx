import React, { useRef, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { cropImage } from '../../actions/images';
import { defaultCropStage } from '../../actions/defaultStageConfigs';
import { preprocessImage } from '../../utils/requests';
import { IMAGE_BOUNDS } from '../../utils/constraints';
import { computeCanvasDimensions } from '../../utils/images';
import { imageTypeProp, imageIndexProp, imageContainerProp } from '../../utils/props';
import 'react-image-crop/dist/ReactCrop.css';
import '../../style/modals/editing-modal.scss';

function CropModal({
  isOpen, setIsOpen, type, index, container,
}) {
  const dispatch = useDispatch();
  const canvasRef = useRef(null);
  const imageViewerRef = useRef(null);

  const imageState = container[index];
  const [hasChanged, setHasChanged] = useState(false);
  const [localState, setLocalState] = useState(imageState.cropStage);
  const [nextColourState, setNextColourState] = useState(imageState.colourStage);
  const [canvasImage, setCanvasImage] = useState(imageState.sizeStage.image);
  const [imageBounds, setImageBounds] = useState(
    {
      minWidth: IMAGE_BOUNDS.minWidth,
      maxWidth: IMAGE_BOUNDS.maxWidth,
      minHeight: IMAGE_BOUNDS.minHeight,
      maxHeight: IMAGE_BOUNDS.maxHeight, // not really relevant until loading the canvas
    },
  );
  const [cropParams, setCropParams] = useState(
    {
      x: null,
      y: null,
      width: null,
      height: null,
    },
  );

  const updateCanvas = () => {
    const imageViewer = imageViewerRef.current;
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');
    const image = new Image();

    image.onload = () => {
      const { newWidth, newHeight } = computeCanvasDimensions({
        imageWidth: image.width,
        imageHeight: image.height,
        viewerWidth: imageViewer.offsetWidth,
        viewerHeight: imageViewer.offsetHeight,
      });
      canvas.width = newWidth;
      canvas.height = newHeight;
      canvas.style.width = `${newWidth}px`;
      canvas.style.height = `${newHeight}px`;
      ctx.drawImage(image, 0, 0, newWidth, newHeight);
      setCanvasImage(canvas.toDataURL());
      setCropParams({
        x: Math.round(localState.x * canvas.width),
        y: Math.round(localState.y * canvas.height),
        width: Math.round(localState.width * canvas.width),
        height: Math.round(localState.height * canvas.height),
      });
    };
    image.src = imageState.sizeStage.image;
  };

  const updateImageBounds = () => {
    const canvas = canvasRef.current;
    setImageBounds({
      minWidth: canvas.width * (IMAGE_BOUNDS.minWidth / imageState.sizeStage.width),
      maxWidth: canvas.width * (IMAGE_BOUNDS.maxWidth / imageState.sizeStage.width),
      minHeight: canvas.height * (IMAGE_BOUNDS.minHeight / imageState.sizeStage.height),
      maxHeight: canvas.height * (IMAGE_BOUNDS.maxHeight / imageState.sizeStage.height),
    });
  };

  useEffect(() => {
    if (canvasRef.current !== null) {
      updateCanvas();
      updateImageBounds();
    }
  }, [canvasRef, imageViewerRef, imageState, localState]);

  const updateCropWindow = (crop) => {
    setCropParams({
      x: crop.x,
      y: crop.y,
      width: crop.width,
      height: crop.height,
    });
  };

  const syncCropWindow = (state) => {
    const canvas = canvasRef.current;
    setCropParams({
      x: Math.round(state.x * canvas.width),
      y: Math.round(state.y * canvas.height),
      width: Math.round(state.width * canvas.width),
      height: Math.round(state.height * canvas.height),
    });
  };

  const getCropWidth = () => Math.round(localState.width * imageState.sizeStage.width);

  const getCropHeight = () => Math.round(localState.height * imageState.sizeStage.height);

  const updateGlobalState = () => {
    dispatch(cropImage(type, index, localState, nextColourState));
  };

  const updateLocalState = (state = null) => {
    let newCropStage;
    if (state === null) { // cropParams dictate the update on state
      const canvas = canvasRef.current;
      newCropStage = { ...imageState.cropStage };
      newCropStage.x = cropParams.x / canvas.width;
      newCropStage.y = cropParams.y / canvas.height;
      newCropStage.width = cropParams.width / canvas.width;
      newCropStage.height = cropParams.height / canvas.height;
    } else { // state dictates the update on cropParams
      newCropStage = state;
      syncCropWindow(state);
    }

    const executions = {
      0: newCropStage,
      1: nextColourState,
    };
    preprocessImage(
      imageState.sizeStage.image,
      imageState.sizeStage.mask,
      executions,
    ).then((response) => {
      setLocalState(response.data.completions[0]);
      setNextColourState(response.data.completions[1]);
    }).catch((error) => {
      console.error(error);
      updateLocalState(defaultCropStage);
    });
    setHasChanged(true);
  };

  const revertLocalState = () => {
    setLocalState(imageState.cropStage);
    syncCropWindow(imageState.cropStage);
    setNextColourState(imageState.colourStage);
    setHasChanged(false);
  };

  const resetLocalState = () => updateLocalState(defaultCropStage);

  const onEnter = () => revertLocalState();

  const onHide = () => setIsOpen(false);

  const onSave = () => {
    updateGlobalState();
    onHide();
  };

  return (
    <Modal
      size="xl"
      show={isOpen}
      onHide={onHide}
      onEnter={onEnter}
      className="editing-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title>
          Crop Image
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="modal-controls">
          <form className="form">
            <p className="row">
              <label className="label" htmlFor="width">
                Crop Width
              </label>
              <input
                className="input"
                id="width"
                type="number"
                disabled="True"
                value={getCropWidth()}
              />
            </p>
            <p className="row">
              <label className="label" htmlFor="height">
                Crop Height
              </label>
              <input
                className="input"
                id="height"
                type="number"
                disabled="True"
                value={getCropHeight()}
              />
            </p>
          </form>
        </div>
        <div className="modal-image" ref={imageViewerRef}>
          <div className="canvas-div">
            <ReactCrop
              className="canvas"
              minWidth={imageBounds.minWidth}
              maxWidth={imageBounds.maxWidth}
              minHeight={imageBounds.minHeight}
              maxHeight={imageBounds.maxHeight}
              src={canvasImage}
              crop={cropParams}
              onImageLoaded={updateCanvas}
              onChange={updateCropWindow}
              onDragEnd={() => updateLocalState()}
            />
            <canvas ref={canvasRef} hidden />
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={resetLocalState}>
          Reset to defaults
        </Button>
        <Button variant="primary" onClick={revertLocalState} disabled={!hasChanged}>
          Revert latest changes
        </Button>
        <Button variant="secondary" onClick={onHide}>
          Close without saving
        </Button>
        <Button variant="success" onClick={onSave} disabled={!hasChanged}>
          Save changes
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

CropModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  setIsOpen: PropTypes.func.isRequired,
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
  container: imageContainerProp.isRequired,
};

export default CropModal;
