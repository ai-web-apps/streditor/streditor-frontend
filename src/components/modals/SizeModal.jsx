import React, { useRef, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { resizeImage } from '../../actions/images';
import { defaultSizeStage } from '../../actions/defaultStageConfigs';
import { preprocessImage } from '../../utils/requests';
import { IMAGE_BOUNDS } from '../../utils/constraints';
import { computeCanvasDimensions } from '../../utils/images';
import { imageTypeProp, imageIndexProp, imageContainerProp } from '../../utils/props';
import '../../style/modals/editing-modal.scss';

function SizeModal({
  isOpen, setIsOpen, type, index, container,
}) {
  const dispatch = useDispatch();
  const canvasRef = useRef(null);
  const imageViewerRef = useRef(null);

  const imageState = container[index];
  const [localState, setLocalState] = useState(imageState.sizeStage);
  const [nextCropState, setNextCropState] = useState(imageState.cropStage);
  const [nextColourState, setNextColourState] = useState(imageState.colourStage);
  const [hasChanged, setHasChanged] = useState(false);

  const updateCanvas = () => {
    const imageViewer = imageViewerRef.current;
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');
    const image = new Image();

    image.onload = () => {
      const { newWidth, newHeight } = computeCanvasDimensions({
        imageWidth: image.width,
        imageHeight: image.height,
        viewerWidth: imageViewer.offsetWidth,
        viewerHeight: imageViewer.offsetHeight,
      });
      canvas.width = newWidth;
      canvas.height = newHeight;
      canvas.style.width = `${newWidth}px`;
      canvas.style.height = `${newHeight}px`;
      ctx.drawImage(image, 0, 0, newWidth, newHeight);
    };
    image.src = localState.image;
  };

  useEffect(() => {
    if (canvasRef.current !== null) {
      updateCanvas();
    }
  }, [canvasRef, imageViewerRef, localState]);

  const updateGlobalState = () => dispatch(
    resizeImage(type, index, localState, nextCropState, nextColourState),
  );

  const updateLocalStateParameters = ({ width = null, height = null }) => {
    if (width !== null) {
      const newHeight = Math.round(
        (width / imageState.inputStage.width) * imageState.inputStage.height,
      );
      setLocalState({ ...localState, width, height: newHeight });
    } else {
      const newWidth = Math.round(
        (height / imageState.inputStage.height) * imageState.inputStage.width,
      );
      setLocalState({ ...localState, height, width: newWidth });
    }
  };

  const updateLocalStateFully = (state) => {
    const executions = {
      0: state,
      1: nextCropState,
      2: nextColourState,
    };
    preprocessImage(
      imageState.inputStage.image,
      imageState.inputStage.mask,
      executions,
    ).then((response) => {
      setLocalState(response.data.completions[0]);
      setNextCropState(response.data.completions[1]);
      setNextColourState(response.data.completions[2]);
    }).catch((error) => {
      console.error(error);
      updateLocalStateFully(defaultSizeStage);
    });
    setHasChanged(true);
  };

  const revertLocalState = () => {
    setLocalState(imageState.sizeStage);
    setNextCropState(imageState.cropStage);
    setNextColourState(imageState.colourStage);
    setHasChanged(false);
  };

  const resetLocalState = () => updateLocalStateFully({
    ...imageState.inputStage, stage: 'size',
  });

  const onEnter = () => {
    updateCanvas();
    revertLocalState();
  };

  const onHide = () => setIsOpen(false);

  const onSave = () => {
    updateGlobalState();
    onHide();
  };

  return (
    <Modal
      size="xl"
      show={isOpen}
      onHide={onHide}
      onEnter={onEnter}
      className="editing-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title>
          Resize Image
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="modal-controls">
          <form
            className="form"
            onSubmit={(event) => {
              if (event) {
                event.preventDefault();
                updateLocalStateFully(localState);
              }
            }}
          >
            <p className="row">
              <label className="label" htmlFor="width">
                Image Width
              </label>
              <input
                className="input"
                id="width"
                type="number"
                min={IMAGE_BOUNDS.minWidth}
                max={IMAGE_BOUNDS.maxWidth}
                step="1"
                value={localState.width}
                onChange={(event) => updateLocalStateParameters(
                  { width: parseInt(event.target.value, 10) },
                )}
              />
            </p>
            <p className="row">
              <label className="label" htmlFor="height">
                Image Height
              </label>
              <input
                className="input"
                id="height"
                type="number"
                min={IMAGE_BOUNDS.minHeight}
                max={IMAGE_BOUNDS.maxHeight}
                step="1"
                value={localState.height}
                onChange={(event) => updateLocalStateParameters(
                  { height: parseInt(event.target.value, 10) },
                )}
              />
            </p>
            <Button type="submit" className="submit">
              Apply
            </Button>
          </form>
        </div>
        <div className="modal-image" ref={imageViewerRef}>
          <div className="canvas-div">
            <canvas className="canvas" ref={canvasRef} />
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={resetLocalState}>
          Reset to defaults
        </Button>
        <Button variant="primary" onClick={revertLocalState} disabled={!hasChanged}>
          Revert latest changes
        </Button>
        <Button variant="secondary" onClick={onHide}>
          Close without saving
        </Button>
        <Button variant="success" onClick={onSave} disabled={!hasChanged}>
          Save changes
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

SizeModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  setIsOpen: PropTypes.func.isRequired,
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
  container: imageContainerProp.isRequired,
};

export default SizeModal;
