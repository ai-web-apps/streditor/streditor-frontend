import React, {
  useRef, useState, useEffect, useCallback,
} from 'react';
import { useDispatch } from 'react-redux';
import { useDropzone } from 'react-dropzone';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Slider, { createSliderWithTooltip } from 'rc-slider';
import { removeMask } from '../../actions/images';
import { colourMask } from '../../utils/requests';
import { MASK_TYPES } from '../../utils/constraints';
import { uploadNewMask, computeCanvasDimensions } from '../../utils/images';
import { imageTypeProp, imageIndexProp, imageContainerProp } from '../../utils/props';
import '../../style/modals/editing-modal.scss';

const SliderWithTooltip = createSliderWithTooltip(Slider);

function MaskModal({
  isOpen, setIsOpen, type, index, container,
}) {
  const dispatch = useDispatch();
  const canvasRef = useRef(null);
  const imageViewerRef = useRef(null);
  const [sliderValue, setSliderValue] = useState(0.6);
  const [maskAlpha, setMaskAlpha] = useState(0.6);
  let imageState = container[index];

  const updateCanvas = () => {
    imageState = container[index];
    const imageViewer = imageViewerRef.current;
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');
    const image = new Image();

    image.onload = () => {
      const { newWidth, newHeight } = computeCanvasDimensions({
        imageWidth: image.width,
        imageHeight: image.height,
        viewerWidth: imageViewer.offsetWidth,
        viewerHeight: imageViewer.offsetHeight,
      });
      canvas.width = newWidth;
      canvas.height = newHeight;
      canvas.style.width = `${newWidth}px`;
      canvas.style.height = `${newHeight}px`;
      ctx.drawImage(image, 0, 0, newWidth, newHeight);

      if (imageState.outputStage.mask !== null) {
        colourMask(imageState.outputStage.mask).then((response) => {
          const colouredMask = new Image();
          colouredMask.onload = () => {
            ctx.globalAlpha = maskAlpha;
            ctx.drawImage(colouredMask, 0, 0, newWidth, newHeight);
          };
          colouredMask.src = response.data.colour_mask;
        }).catch((error) => console.error(error));
      }
    };
    image.src = imageState.outputStage.image;
  };

  useEffect(() => {
    if (canvasRef.current !== null) {
      updateCanvas();
    }
  }, [canvasRef, imageViewerRef, container[index], maskAlpha]);

  const onDrop = useCallback(
    (files) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        uploadNewMask(type, index, imageState, event.target.result, dispatch);
      };
      reader.readAsDataURL(files[0]);
    },
    [type, imageState],
  );

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: MASK_TYPES,
    maxFiles: 1,
  });

  const onRemove = () => dispatch(removeMask(type, index));

  const onEnter = () => updateCanvas();

  const onHide = () => setIsOpen(false);

  return (
    <Modal
      size="lg"
      show={isOpen}
      onHide={onHide}
      onEnter={onEnter}
      className="editing-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title>
          Upload Image Mask
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="modal-image" ref={imageViewerRef}>
          <div className="canvas-div">
            <canvas className="canvas" ref={canvasRef} />
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <SliderWithTooltip
          className="mask-alpha-slider"
          min={0}
          max={1}
          step={0.02}
          value={sliderValue}
          onChange={(value) => setSliderValue(value)}
          onAfterChange={() => { setMaskAlpha(sliderValue); }}
          disabled={imageState.outputStage.mask === null}
        />
        <Button
          variant="danger"
          disabled={imageState.outputStage.mask === null}
          onClick={onRemove}
        >
          Remove Mask
        </Button>
        <Button variant="secondary" onClick={onHide}>
          Close
        </Button>
        <Button {...getRootProps({ variant: 'success' })}>
          <input {...getInputProps()} />
          Upload New Mask
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

MaskModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  setIsOpen: PropTypes.func.isRequired,
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
  container: imageContainerProp.isRequired,
};

export default MaskModal;
