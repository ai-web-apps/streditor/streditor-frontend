import React, { useState } from 'react';
import PropTypes, { oneOfType } from 'prop-types';
import Alert from 'react-bootstrap/Alert';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import { vggLayersProp } from '../../utils/props';
import '../../style/modals/vgg-modal.scss';

function VggModal({
  isOpen, setIsOpen, initialState, defaultState, updateCallback, layerDisabled,
}) {
  const [localState, setLocalState] = useState(initialState);
  const [hasChanged, setHasChanged] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const layerOn = (layerName) => {
    if (localState === undefined) {
      return false;
    }
    if (typeof localState === 'string') {
      return localState === layerName;
    }
    return localState[layerName] !== undefined;
  };

  const updateGlobalState = () => updateCallback(localState);

  const updateLocalState = ({ layerName = null, state = null }) => {
    if (state !== null) { // update the state of all layers
      setLocalState(state);
    } else if (layerName !== null) { // update by switching the state of one layer
      if (typeof localState === 'string') {
        setLocalState(layerName);
      } else {
        const newState = { ...localState };
        if (localState[layerName] === undefined) {
          newState[layerName] = 1;
        } else {
          delete newState[layerName];
        }
        setLocalState(newState);
      }
    }
    setHasChanged(true);
  };

  const revertLocalState = () => {
    setHasChanged(false);
    setLocalState(initialState);
  };

  const resetLocalState = () => updateLocalState({ state: defaultState });

  const onEnter = () => revertLocalState();

  const onHide = () => { setIsOpen(false); setShowAlert(false); };

  const onSave = () => {
    if (typeof localState === 'string' || Object.keys(localState).length) {
      updateGlobalState();
      onHide();
    } else {
      setShowAlert(true);
    }
  };

  const layerButtons = [
    [1, 1], [1, 2], ['divider', 1],
    [2, 1], [2, 2], ['divider', 2],
    [3, 1], [3, 2], [3, 3], [3, 4], ['divider', 3],
    [4, 1], [4, 2], [4, 3], [4, 4], ['divider', 4],
    [5, 1], [5, 2], [5, 3], [5, 4],
  ].map((x) => {
    if (x[0] === 'divider') {
      return (
        <pre type="text" key={x[1]}> </pre>
      );
    }
    const layerName = `block${x[0]}_conv${x[1]}`;
    return (
      <Button
        size="sm"
        key={layerName}
        className="layer-button"
        variant={layerOn(layerName) ? 'primary' : 'dark'}
        disabled={layerDisabled(layerName)}
        onClick={() => updateLocalState({ layerName })}
      >
        <div className="button-text">{layerName}</div>
      </Button>
    );
  });

  return (
    <Modal
      size="lg"
      show={isOpen}
      onHide={onHide}
      onEnter={onEnter}
      className="vgg-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title>
          VGG19 layer selection
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ButtonGroup
          aria-label="Layers"
          className="layers-group"
        >
          {layerButtons}
        </ButtonGroup>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={resetLocalState}>
          Reset to defaults
        </Button>
        <Button variant="primary" onClick={revertLocalState} disabled={!hasChanged}>
          Revert latest changes
        </Button>
        <Button variant="secondary" onClick={onHide}>
          Close without saving
        </Button>
        <Button variant="success" onClick={onSave} disabled={!hasChanged}>
          Save changes
        </Button>
      </Modal.Footer>
      <Alert variant="danger" show={showAlert} onClose={() => setShowAlert(false)} dismissible>
        <Alert.Heading>Error!</Alert.Heading>
        <p>
          At least one layer has to be selected.
        </p>
      </Alert>
    </Modal>
  );
}

VggModal.defaultProps = {
  layerDisabled: () => false,
};

VggModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  setIsOpen: PropTypes.func.isRequired,
  initialState: oneOfType([PropTypes.string, vggLayersProp]).isRequired,
  defaultState: oneOfType([PropTypes.string, vggLayersProp]).isRequired,
  updateCallback: PropTypes.func.isRequired,
  layerDisabled: PropTypes.func,
};

export default VggModal;
