import React, { useRef, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import SliderWrap from '../input-elements/Slider';
import { recolourImage } from '../../actions/images';
import { defaultColourStage } from '../../actions/defaultStageConfigs';
import { preprocessImage } from '../../utils/requests';
import { computeCanvasDimensions } from '../../utils/images';
import { imageTypeProp, imageIndexProp, imageContainerProp } from '../../utils/props';
import '../../style/modals/editing-modal.scss';

function ColourModal({
  isOpen, setIsOpen, type, index, container,
}) {
  const dispatch = useDispatch();
  const canvasRef = useRef(null);
  const imageViewerRef = useRef(null);

  const imageState = container[index];
  const [localState, setLocalState] = useState(imageState.colourStage);
  const [hasChanged, setHasChanged] = useState(false);

  const updateCanvas = () => {
    const imageViewer = imageViewerRef.current;
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');
    const image = new Image();

    image.onload = () => {
      const { newWidth, newHeight } = computeCanvasDimensions({
        imageWidth: image.width,
        imageHeight: image.height,
        viewerWidth: imageViewer.offsetWidth,
        viewerHeight: imageViewer.offsetHeight,
      });
      canvas.width = newWidth;
      canvas.height = newHeight;
      canvas.style.width = `${newWidth}px`;
      canvas.style.height = `${newHeight}px`;
      ctx.drawImage(image, 0, 0, newWidth, newHeight);
    };
    image.src = localState.image;
  };

  useEffect(() => {
    if (canvasRef.current !== null) {
      updateCanvas();
    }
  }, [canvasRef, imageViewerRef, localState]);

  const updateGlobalState = () => dispatch(recolourImage(type, index, localState));

  const updateLocalStateParameters = (key, value) => {
    setLocalState({ ...localState, [key]: value });
  };

  const updateLocalStateFully = (state) => {
    const executions = { 0: state };
    preprocessImage(
      imageState.cropStage.image,
      imageState.cropStage.mask,
      executions,
    ).then((response) => {
      setLocalState(response.data.completions[0]);
    }).catch((error) => {
      console.error(error);
      updateLocalStateFully(defaultColourStage);
    });
    setHasChanged(true);
  };

  const revertLocalState = () => {
    setLocalState(imageState.colourStage);
    setHasChanged(false);
  };

  const resetLocalState = () => updateLocalStateFully(defaultColourStage);

  const onEnter = () => {
    updateCanvas();
    revertLocalState();
  };

  const onHide = () => setIsOpen(false);

  const onSave = () => {
    updateGlobalState();
    onHide();
  };

  return (
    <Modal
      size="xl"
      show={isOpen}
      onHide={onHide}
      onEnter={onEnter}
      className="editing-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title>
          Edit Colour
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="modal-controls">
          <SliderWrap
            name="brightness"
            min={0}
            max={100}
            step={1}
            value={localState.brightness}
            onChange={updateLocalStateParameters}
            onAfterChange={() => updateLocalStateFully(localState)}
          />
          <SliderWrap
            name="contrast"
            min={0}
            max={100}
            step={1}
            value={localState.contrast}
            onChange={updateLocalStateParameters}
            onAfterChange={() => updateLocalStateFully(localState)}
          />
          <SliderWrap
            name="red"
            min={0}
            max={100}
            step={1}
            value={localState.red}
            onChange={updateLocalStateParameters}
            onAfterChange={() => updateLocalStateFully(localState)}
          />
          <SliderWrap
            name="green"
            min={0}
            max={100}
            step={1}
            value={localState.green}
            onChange={updateLocalStateParameters}
            onAfterChange={() => updateLocalStateFully(localState)}
          />
          <SliderWrap
            name="blue"
            min={0}
            max={100}
            step={1}
            value={localState.blue}
            onChange={updateLocalStateParameters}
            onAfterChange={() => updateLocalStateFully(localState)}
          />
        </div>
        <div className="modal-image" ref={imageViewerRef}>
          <div className="canvas-div">
            <canvas className="canvas" ref={canvasRef} />
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={resetLocalState}>
          Reset to defaults
        </Button>
        <Button variant="primary" onClick={revertLocalState} disabled={!hasChanged}>
          Revert latest changes
        </Button>
        <Button variant="secondary" onClick={onHide}>
          Close without saving
        </Button>
        <Button variant="success" onClick={onSave} disabled={!hasChanged}>
          Save changes
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

ColourModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  setIsOpen: PropTypes.func.isRequired,
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
  container: imageContainerProp.isRequired,
};

export default ColourModal;
