import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { ImImages } from 'react-icons/im';
import { GiPalette } from 'react-icons/gi';
import Accordion from 'react-bootstrap/Accordion';
import GatysCard from './sidebar-cards/GatysCard';
import YijunCard from './sidebar-cards/YijunCard';
import ReinhardCard from './sidebar-cards/ReinhardCard';
import { masksValidation } from '../utils/images';
import '../style/sidebar.scss';

function Sidebar() {
  const baseIndex = useSelector((state) => state.baseIndex);
  const baseContainer = useSelector((state) => state.baseContainer);
  const styleIndex = useSelector((state) => state.styleIndex);
  const styleContainer = useSelector((state) => state.styleContainer);
  const synthesizedContainer = useSelector((state) => state.synthesizedContainer);
  const [masksValid, setMasksValid] = useState(true);

  const baseState = baseIndex === null
    || !Object.keys(baseContainer).length
    || !(baseIndex in baseContainer)
    ? null : baseContainer[baseIndex];
  const styleState = styleIndex === null
    || !Object.keys(styleContainer).length
    || !(styleIndex in styleContainer)
    ? null : styleContainer[styleIndex];

  useEffect(() => {
    if (baseState !== null && styleState !== null) {
      masksValidation(baseState.outputStage.mask, styleState.outputStage.mask, setMasksValid);
    }
  }, [baseState, styleState]);

  return (
    <div className="sidebar">
      <Accordion>

        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <div id="icon"><GiPalette /></div>
            <div id="title">Gatys Method</div>
          </Accordion.Header>
          <Accordion.Body>
            <GatysCard
              baseState={baseState}
              styleState={styleState}
              synthesizedContainer={synthesizedContainer}
              masksValid={masksValid}
            />
          </Accordion.Body>
        </Accordion.Item>

        <Accordion.Item eventKey="1">
          <Accordion.Header>
            <div id="icon"><ImImages /></div>
            <div id="title">Yijun Method</div>
          </Accordion.Header>
          <Accordion.Body>
            <YijunCard
              baseState={baseState}
              styleState={styleState}
              synthesizedContainer={synthesizedContainer}
              masksValid={masksValid}
            />
          </Accordion.Body>
        </Accordion.Item>

        <Accordion.Item eventKey="2">
          <Accordion.Header>
            <div id="icon"><ImImages /></div>
            <div id="title">Reinhard Method</div>
          </Accordion.Header>
          <Accordion.Body>
            <ReinhardCard
              baseState={baseState}
              styleState={styleState}
              synthesizedContainer={synthesizedContainer}
              masksValid={masksValid}
            />
          </Accordion.Body>
        </Accordion.Item>

      </Accordion>
    </div>
  );
}

export default Sidebar;
