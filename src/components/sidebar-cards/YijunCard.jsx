import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import Tooltip from 'react-bootstrap/Tooltip';
import Dropdown from 'react-bootstrap/Dropdown';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import VggModal from '../modals/VggModal';
import changeAppState from '../../actions/app';
import { changeIndex } from '../../actions/images';
import { addHistoryRecord } from '../../actions/history';
import { orderStyleTransfer } from '../../utils/requests';
import { imageStateProp, imageContainerProp } from '../../utils/props';
import { loadExistingImage, getIndexFromHash, getRunDisabledReason } from '../../utils/images';
import '../../style/sidebar-card.scss';

function YijunCard({
  baseState, styleState, synthesizedContainer, masksValid,
}) {
  const defaultMaConfig = {
    lambda_: 1e-5,
    epsilon: 1e-7,
    window_radius: 1,
  };
  const defaultGfConfig = {
    epsilon: 10,
    window_radius: 21,
  };
  const defaultPartialConfig = {
    method: 'yijun',
    style_strength: 1.0,
    style_bottom_layer: 'block5_conv2',
    smoothing_tool: 'matting_affinity',
  };

  const dispatch = useDispatch();
  const appState = useSelector((state) => state.app);
  const globalStylingConfig = useSelector((state) => state.yijun);
  const [maConfig, setMaConfig] = useState(defaultMaConfig);
  const [gfConfig, setGfConfig] = useState(defaultGfConfig);
  const [partialConfig, setPartialConfig] = useState(defaultPartialConfig);
  const [tags, setTags] = useState('');

  const runDisabledReason = getRunDisabledReason(baseState, styleState, masksValid, appState);

  useEffect(() => {
    if (globalStylingConfig !== undefined && globalStylingConfig !== null) {
      setPartialConfig(
        Object.keys(globalStylingConfig)
          .filter((key) => key in partialConfig)
          .reduce((obj, key) => ({ ...obj, [key]: globalStylingConfig[key] }), {}),
      );
      if (globalStylingConfig.smoothing_tool === 'matting_affinity') {
        setMaConfig(globalStylingConfig.smoothing);
      } else if (globalStylingConfig.smoothing_tool === 'guided_filter') {
        setGfConfig(globalStylingConfig.smoothing);
      }
    }
  }, [globalStylingConfig]);

  const smoothingTools = {
    off: 'Off',
    matting_affinity: 'Matting Affinity',
    guided_filter: 'Guided Filter',
  };
  const smoothingItems = Object.keys(smoothingTools).map((key) => (
    <Dropdown.Item
      key={key}
      onClick={() => setPartialConfig({ ...partialConfig, smoothing_tool: key })}
    >
      {smoothingTools[key]}
    </Dropdown.Item>
  ));

  const [layerModalIsOpen, setLayerModalIsOpen] = useState(false);
  const layerDisabled = (layerName) => ![1, 2, 3, 4, 5].map((x) => `block${x}_conv2`).includes(layerName);
  const layerUpdateCallback = (layerName) => {
    setPartialConfig({ ...partialConfig, style_bottom_layer: layerName });
  };

  const onSubmit = (event) => {
    if (event) {
      event.preventDefault();
    }
    dispatch(changeAppState('processing'));

    let stylingConfig;
    switch (partialConfig.smoothing_tool) {
      case 'matting_affinity':
        stylingConfig = { ...partialConfig, smoothing: maConfig };
        break;
      case 'guided_filter':
        stylingConfig = { ...partialConfig, smoothing: gfConfig };
        break;
      default:
        stylingConfig = partialConfig;
    }

    orderStyleTransfer(
      baseState.outputStage.image,
      styleState.outputStage.image,
      baseState.outputStage.mask,
      styleState.outputStage.mask,
      stylingConfig,
    ).then((response) => {
      const date = new Date();
      const { image, hash } = response.data;
      const index = getIndexFromHash(synthesizedContainer, hash);

      dispatch(changeIndex('synthesized', index));
      loadExistingImage('synthesized', index, image, hash, dispatch);
      dispatch(changeAppState('idle'));
      dispatch(addHistoryRecord(
        tags, date, hash, baseState, styleState, image, stylingConfig,
      ));
    });
  };

  return (
    <form className="configs-form" onSubmit={onSubmit}>
      <div className="table">
        <div className="row">
          <label className="label" htmlFor="style-strength">
            Style strength
          </label>
          <input
            className="input"
            id="style-strength"
            type="number"
            min="0"
            max="1"
            step="1e-2"
            value={partialConfig.style_strength}
            onChange={(event) => setPartialConfig(
              { ...partialConfig, style_strength: parseFloat(event.target.value) },
            )}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="style-bottom-layer">
            Style bottom layer
          </label>
          <Button
            variant="secondary"
            className="toggle"
            id="style-bottom-layer"
            onClick={() => setLayerModalIsOpen(true)}
          >
            {partialConfig.style_bottom_layer}
          </Button>
        </div>
        <VggModal
          isOpen={layerModalIsOpen}
          setIsOpen={setLayerModalIsOpen}
          initialState={partialConfig.style_bottom_layer}
          defaultState={defaultPartialConfig.style_bottom_layer}
          updateCallback={layerUpdateCallback}
          layerDisabled={layerDisabled}
        />

        <div className="row">
          <label className="label" htmlFor="smoothing">
            Smoothing
          </label>
          <Dropdown drop="down">
            <Dropdown.Toggle id="smoothing" variant="secondary">
              {smoothingTools[partialConfig.smoothing_tool]}
            </Dropdown.Toggle>
            <Dropdown.Menu align="start" variant="secondary">
              {smoothingItems}
            </Dropdown.Menu>
          </Dropdown>
        </div>

        <div className="row">
          <label
            htmlFor="ma-lambda"
            className={
              partialConfig.smoothing_tool === 'matting_affinity' ? 'label' : 'label-disabled'
            }
          >
            MA lambda
          </label>
          <input
            className={
              partialConfig.smoothing_tool === 'matting_affinity' ? 'input' : 'input-disabled'
            }
            id="ma-lambda"
            type="number"
            min="0"
            max="1"
            step="1e-6"
            disabled={partialConfig.smoothing_tool !== 'matting_affinity'}
            value={maConfig.lambda_}
            onChange={(event) => {
              setMaConfig({ ...maConfig, lambda_: parseFloat(event.target.value) });
            }}
          />
        </div>

        <div className="row">
          <label
            htmlFor="ma-epsilon"
            className={
              partialConfig.smoothing_tool === 'matting_affinity' ? 'label' : 'label-disabled'
            }
          >
            MA epsilon
          </label>
          <input
            className={
              partialConfig.smoothing_tool === 'matting_affinity' ? 'input' : 'input-disabled'
            }
            id="ma-epsilon"
            type="number"
            min="0"
            max="1e-4"
            step="1e-8"
            disabled={partialConfig.smoothing_tool !== 'matting_affinity'}
            value={maConfig.epsilon}
            onChange={(event) => {
              setMaConfig({ ...maConfig, epsilon: parseFloat(event.target.value) });
            }}
          />
        </div>

        <div className="row">
          <label
            htmlFor="ma-window-radius"
            className={
              partialConfig.smoothing_tool === 'matting_affinity' ? 'label' : 'label-disabled'
            }
          >
            MA window radius
          </label>
          <input
            className={
              partialConfig.smoothing_tool === 'matting_affinity' ? 'input' : 'input-disabled'
            }
            id="ma-window-radius"
            type="number"
            min="1"
            max="9"
            step="2"
            disabled={partialConfig.smoothing_tool !== 'matting_affinity'}
            value={maConfig.window_radius}
            onChange={(event) => {
              setMaConfig({ ...maConfig, window_radius: parseInt(event.target.value, 10) });
            }}
          />
        </div>

        <div className="row">
          <label
            htmlFor="gf-epsilon"
            className={
              partialConfig.smoothing_tool === 'guided_filter' ? 'label' : 'label-disabled'
            }
          >
            GF epsilon
          </label>
          <input
            className={
              partialConfig.smoothing_tool === 'guided_filter' ? 'input' : 'input-disabled'
            }
            id="gf-epsilon"
            type="number"
            min="0"
            max="100"
            step="0.1"
            disabled={partialConfig.smoothing_tool !== 'guided_filter'}
            value={gfConfig.epsilon}
            onChange={(event) => {
              setGfConfig({ ...gfConfig, epsilon: parseFloat(event.target.value) });
            }}
          />
        </div>

        <div className="row">
          <label
            htmlFor="gf-window-radius"
            className={
              partialConfig.smoothing_tool === 'guided_filter' ? 'label' : 'label-disabled'
            }
          >
            GF window radius
          </label>
          <input
            className={
              partialConfig.smoothing_tool === 'guided_filter' ? 'input' : 'input-disabled'
            }
            id="gf-window-radius"
            type="number"
            min="1"
            max="51"
            step="2"
            disabled={partialConfig.smoothing_tool !== 'guided_filter'}
            value={gfConfig.window_radius}
            onChange={(event) => {
              setGfConfig({ ...gfConfig, window_radius: parseInt(event.target.value, 10) });
            }}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="tags-yijun">
            <div className="tags-sublabel">Tags (optional)</div>
          </label>
          <input
            className="tags-input"
            id="tags-yijun"
            maxLength="20"
            value={tags}
            onChange={(event) => setTags(event.target.value)}
          />
        </div>
      </div>

      <div className="buttons">
        <span className="button-block">
          <Button
            size="lg"
            variant="secondary"
            className="button"
            onClick={() => {
              setMaConfig(defaultMaConfig);
              setGfConfig(defaultGfConfig);
              setPartialConfig(defaultPartialConfig);
            }}
          >
            Reset
          </Button>
        </span>
        {runDisabledReason === 'OK'
          ? (
            <span className="button-block">
              <Button
                size="lg"
                type="submit"
                variant="success"
                className="button"
              >
                Run
              </Button>
            </span>
          )
          : (
            <OverlayTrigger overlay={(<Tooltip>{runDisabledReason}</Tooltip>)}>
              <span className="button-block">
                <Button
                  size="lg"
                  type="submit"
                  variant="success"
                  className="button"
                  disabled
                  style={{ pointerEvents: 'none' }}
                >
                  Run
                </Button>
              </span>
            </OverlayTrigger>
          )}
      </div>

    </form>
  );
}

YijunCard.defaultProps = {
  baseState: null,
  styleState: null,
  synthesizedContainer: null,
};

YijunCard.propTypes = {
  baseState: imageStateProp,
  styleState: imageStateProp,
  synthesizedContainer: imageContainerProp,
  masksValid: PropTypes.bool.isRequired,
};

export default YijunCard;
