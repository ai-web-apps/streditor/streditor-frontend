import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import changeAppState from '../../actions/app';
import { changeIndex } from '../../actions/images';
import { addHistoryRecord } from '../../actions/history';
import { orderStyleTransfer } from '../../utils/requests';
import { imageStateProp, imageContainerProp } from '../../utils/props';
import { loadExistingImage, getIndexFromHash, getRunDisabledReason } from '../../utils/images';
import '../../style/sidebar-card.scss';

function ReinhardCard({
  baseState, styleState, synthesizedContainer, masksValid,
}) {
  const defaultStylingConfig = { method: 'reinhard', style_strength: 1.0 };

  const dispatch = useDispatch();
  const appState = useSelector((state) => state.app);
  const globalStylingConfig = useSelector((state) => state.reinhard);
  const [stylingConfig, setStylingConfig] = useState(defaultStylingConfig);
  const [tags, setTags] = useState('');

  const runDisabledReason = getRunDisabledReason(baseState, styleState, masksValid, appState);

  useEffect(() => {
    if (globalStylingConfig !== undefined && globalStylingConfig !== null) {
      setStylingConfig(globalStylingConfig);
    }
  }, [globalStylingConfig]);

  const onSubmit = (event) => {
    if (event) {
      event.preventDefault();
    }
    dispatch(changeAppState('processing'));
    orderStyleTransfer(
      baseState.outputStage.image,
      styleState.outputStage.image,
      baseState.outputStage.mask,
      styleState.outputStage.mask,
      stylingConfig,
    ).then((response) => {
      const date = new Date();
      const { image, hash } = response.data;
      const index = getIndexFromHash(synthesizedContainer, hash);

      dispatch(changeIndex('synthesized', index));
      loadExistingImage('synthesized', index, image, hash, dispatch);
      dispatch(changeAppState('idle'));
      dispatch(addHistoryRecord(tags, date, hash, baseState, styleState, image, stylingConfig));
    });
  };

  return (
    <form className="configs-form" onSubmit={onSubmit}>
      <div className="table">
        <div className="row">
          <label className="label" htmlFor="style-strength">
            Style strength
          </label>
          <input
            className="input"
            id="style-strength"
            type="number"
            min="0"
            max="1"
            step="1e-2"
            value={stylingConfig.style_strength}
            onChange={(event) => setStylingConfig(
              { ...stylingConfig, style_strength: parseFloat(event.target.value) },
            )}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="tags-reinhard">
            <div className="tags-sublabel">Tags (optional)</div>
          </label>
          <input
            className="tags-input"
            id="tags-reinhard"
            maxLength="20"
            value={tags}
            onChange={(event) => setTags(event.target.value)}
          />
        </div>
      </div>

      <div className="buttons">
        <span className="button-block">
          <Button
            size="lg"
            variant="secondary"
            className="button"
            onClick={() => setStylingConfig(defaultStylingConfig)}
          >
            Reset
          </Button>
        </span>
        {runDisabledReason === 'OK'
          ? (
            <span className="button-block">
              <Button
                size="lg"
                type="submit"
                variant="success"
                className="button"
              >
                Run
              </Button>
            </span>
          )
          : (
            <OverlayTrigger overlay={(<Tooltip>{runDisabledReason}</Tooltip>)}>
              <span className="button-block">
                <Button
                  size="lg"
                  type="submit"
                  variant="success"
                  className="button"
                  disabled
                  style={{ pointerEvents: 'none' }}
                >
                  Run
                </Button>
              </span>
            </OverlayTrigger>
          )}
      </div>

    </form>
  );
}

ReinhardCard.defaultProps = {
  baseState: null,
  styleState: null,
  synthesizedContainer: null,
};

ReinhardCard.propTypes = {
  baseState: imageStateProp,
  styleState: imageStateProp,
  synthesizedContainer: imageContainerProp,
  masksValid: PropTypes.bool.isRequired,
};

export default ReinhardCard;
