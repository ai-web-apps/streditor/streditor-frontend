import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Toggle from 'react-toggle';
import Button from 'react-bootstrap/Button';
import Tooltip from 'react-bootstrap/Tooltip';
import Dropdown from 'react-bootstrap/Dropdown';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import VggModal from '../modals/VggModal';
import changeAppState from '../../actions/app';
import { changeIndex } from '../../actions/images';
import { addHistoryRecord } from '../../actions/history';
import { orderStyleTransfer } from '../../utils/requests';
import { imageStateProp, imageContainerProp } from '../../utils/props';
import { loadExistingImage, getIndexFromHash, getRunDisabledReason } from '../../utils/images';
import 'react-toggle/style.css';
import '../../style/sidebar-card.scss';

function GatysCard({
  baseState, styleState, synthesizedContainer, masksValid,
}) {
  const defaultContentLayers = {
    block5_conv2: 1.0,
  };
  const defaultStyleLayers = {
    block1_conv2: 1.0,
    block2_conv2: 1.0,
    block3_conv2: 1.0,
    block4_conv2: 1.0,
    block5_conv2: 1.0,
  };
  const defaultStylingConfig = {
    method: 'gatys',
    max_iterations: 1000,
    early_stopping: {
      enabled: false,
      delta: 0.001,
      patience: 100,
    },
    optimizer: 'Adam',
    optimizer_params: {},
    learning_rate_params: {
      decay_steps: 1000,
      initial_learning_rate: 5.0,
      end_learning_rate: 2.0,
      power: 1.5,
    },
    content_layers: defaultContentLayers,
    style_layers: defaultStyleLayers,
    content_loss_weight: 1.0,
    style_loss_weight: 10.0,
    variation_loss_weight: 0.002,
  };

  const dispatch = useDispatch();
  const appState = useSelector((state) => state.app);
  const globalStylingConfig = useSelector((state) => state.gatys);
  const [stylingConfig, setStylingConfig] = useState(defaultStylingConfig);
  const [tags, setTags] = useState('');

  const runDisabledReason = getRunDisabledReason(baseState, styleState, masksValid, appState);

  useEffect(() => {
    if (globalStylingConfig !== undefined && globalStylingConfig !== null) {
      setStylingConfig(globalStylingConfig);
    }
  }, [globalStylingConfig]);

  const optimizers = ['Adagrad', 'Adadelta', 'Adam', 'Adamax', 'RMSprop', 'SGD'];
  const optimizersItems = optimizers.map((name) => (
    <Dropdown.Item
      key={name}
      onClick={() => setStylingConfig({ ...stylingConfig, optimizer: name })}
    >
      {name}
    </Dropdown.Item>
  ));

  const contentLayers = Object.keys(stylingConfig.content_layers).sort().map((layer) => (
    <div className="row" key={layer}>
      <label className="label" htmlFor={`content-${layer}`}>
        <pre type="text" className="custom-label-content">{`  • ${layer} weight`}</pre>
      </label>
      <input
        className="input"
        id={`content-${layer}`}
        type="number"
        min="0"
        max="1e5"
        step="1e-3"
        value={stylingConfig.content_layers[layer]}
        onChange={(event) => setStylingConfig({
          ...stylingConfig,
          content_layers: {
            ...stylingConfig.content_layers,
            [layer]: parseFloat(event.target.value),
          },
        })}
      />
    </div>
  ));

  const styleLayers = Object.keys(stylingConfig.style_layers).sort().map((layer) => (
    <div className="row" key={layer}>
      <label className="label" htmlFor={`style-${layer}`}>
        <pre type="text" className="custom-label-style">{`  • ${layer} weight`}</pre>
      </label>
      <input
        className="input"
        id={`style-${layer}`}
        type="number"
        min="0"
        max="1e5"
        step="1e-3"
        value={stylingConfig.style_layers[layer]}
        onChange={(event) => setStylingConfig({
          ...stylingConfig,
          style_layers: {
            ...stylingConfig.style_layers,
            [layer]: parseFloat(event.target.value),
          },
        })}
      />
    </div>
  ));

  const [contentModalIsOpen, setContentModalIsOpen] = useState(false);
  const [styleModalIsOpen, setStyleModalIsOpen] = useState(false);

  const contentLayerCallback = (layers) => {
    setStylingConfig({ ...stylingConfig, content_layers: layers });
  };
  const styleLayerCallback = (layers) => {
    setStylingConfig({ ...stylingConfig, style_layers: layers });
  };

  const onSubmit = (event) => {
    if (event) {
      event.preventDefault();
    }
    dispatch(changeAppState('processing'));
    orderStyleTransfer(
      baseState.outputStage.image,
      styleState.outputStage.image,
      baseState.outputStage.mask,
      styleState.outputStage.mask,
      stylingConfig,
    ).then((response) => {
      const date = new Date();
      const { image, hash } = response.data;
      const index = getIndexFromHash(synthesizedContainer, hash);

      dispatch(changeIndex('synthesized', index));
      loadExistingImage('synthesized', index, image, hash, dispatch);
      dispatch(changeAppState('idle'));
      dispatch(addHistoryRecord(tags, date, hash, baseState, styleState, image, stylingConfig));
    });
  };

  return (
    <form className="configs-form" onSubmit={onSubmit}>
      <div className="table">
        <div className="row">
          <label className="label" htmlFor="max-iterations">
            Iterations
          </label>
          <input
            className="input"
            id="max-iterations"
            type="number"
            min="100"
            max="30000"
            step="100"
            value={stylingConfig.max_iterations}
            onChange={(event) => setStylingConfig(
              { ...stylingConfig, max_iterations: parseInt(event.target.value, 10) },
            )}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="content-loss-weight">
            Content loss weight
          </label>
          <input
            className="input"
            id="content-loss-weight"
            type="number"
            min="0"
            max="1e5"
            step="1e-3"
            value={stylingConfig.content_loss_weight}
            onChange={(event) => setStylingConfig(
              { ...stylingConfig, content_loss_weight: parseFloat(event.target.value) },
            )}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="style-loss-weight">
            Style loss weight
          </label>
          <input
            className="input"
            id="style-loss-weight"
            type="number"
            min="0"
            max="1e5"
            step="1e-3"
            value={stylingConfig.style_loss_weight}
            onChange={(event) => setStylingConfig(
              { ...stylingConfig, style_loss_weight: parseFloat(event.target.value) },
            )}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="variation-loss-weight">
            Variation loss weight
          </label>
          <input
            className="input"
            id="variation-loss-weight"
            type="number"
            min="0"
            max="1"
            step="1e-3"
            value={stylingConfig.variation_loss_weight}
            onChange={(event) => setStylingConfig(
              { ...stylingConfig, variation_loss_weight: parseFloat(event.target.value) },
            )}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="content-colour" id="content-colour">
            Content loss layers
          </label>
          <Button
            variant="secondary"
            className="toggle"
            id="content-colour"
            onClick={() => setContentModalIsOpen(true)}
          >
            Select
          </Button>
        </div>
        <VggModal
          isOpen={contentModalIsOpen}
          setIsOpen={setContentModalIsOpen}
          initialState={stylingConfig.content_layers}
          defaultState={defaultContentLayers}
          updateCallback={contentLayerCallback}
        />
        {contentLayers}

        <div className="row">
          <label className="label" htmlFor="style-colour" id="style-colour">
            Style loss layers
          </label>
          <Button
            variant="secondary"
            className="toggle"
            id="style-colour"
            onClick={() => setStyleModalIsOpen(true)}
          >
            Select
          </Button>
        </div>
        <VggModal
          isOpen={styleModalIsOpen}
          setIsOpen={setStyleModalIsOpen}
          initialState={stylingConfig.style_layers}
          defaultState={defaultStyleLayers}
          updateCallback={styleLayerCallback}
        />
        {styleLayers}

        <div className="row">
          <label className="label" htmlFor="optimizer">
            Optimizer
          </label>
          <Dropdown drop="down">
            <Dropdown.Toggle id="optimizer" variant="secondary">
              {stylingConfig.optimizer}
            </Dropdown.Toggle>
            <Dropdown.Menu align="start" variant="secondary">
              {optimizersItems}
            </Dropdown.Menu>
          </Dropdown>
        </div>

        <div className="row">
          <label className="label" htmlFor="learning-rate-initial">
            Learning rate initial
          </label>
          <input
            className="input"
            id="learning-rate-initial"
            type="number"
            min="0"
            max="1e5"
            step="1e-5"
            value={stylingConfig.learning_rate_params.initial_learning_rate}
            onChange={(event) => setStylingConfig({
              ...stylingConfig,
              learning_rate_params: {
                ...stylingConfig.learning_rate_params,
                initial_learning_rate: parseFloat(event.target.value),
              },
            })}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="learning-rate-end">
            Learning rate final
          </label>
          <input
            className="input"
            id="learning-rate-end"
            type="number"
            min="0"
            max="1e5"
            step="1e-5"
            value={stylingConfig.learning_rate_params.end_learning_rate}
            onChange={(event) => setStylingConfig({
              ...stylingConfig,
              learning_rate_params: {
                ...stylingConfig.learning_rate_params,
                end_learning_rate: parseFloat(event.target.value),
              },
            })}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="learning-decay-steps">
            Learning rate decay steps
          </label>
          <input
            className="input"
            id="learning-decay-steps"
            type="number"
            min="10"
            max="100000"
            step="10"
            value={stylingConfig.learning_rate_params.decay_steps}
            onChange={(event) => setStylingConfig({
              ...stylingConfig,
              learning_rate_params: {
                ...stylingConfig.learning_rate_params,
                decay_steps: parseInt(event.target.value, 10),
              },
            })}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="learning-decay-power">
            Learning rate decay power
          </label>
          <input
            className="input"
            id="learning-decay-power"
            type="number"
            min="0.1"
            max="10"
            step="0.01"
            value={stylingConfig.learning_rate_params.power}
            onChange={(event) => setStylingConfig({
              ...stylingConfig,
              learning_rate_params: {
                ...stylingConfig.learning_rate_params,
                power: parseFloat(event.target.value),
              },
            })}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="early-stopping">
            Early stopping
          </label>
          <div className="single-toggle">
            <Toggle
              id="early-stopping"
              checked={stylingConfig.early_stopping.enabled}
              onChange={(event) => setStylingConfig({
                ...stylingConfig,
                early_stopping: {
                  ...stylingConfig.early_stopping,
                  enabled: event.target.checked,
                },
              })}
            />
          </div>
        </div>

        <div className="row">
          <label
            htmlFor="early-stopping-delta"
            className={
              stylingConfig.early_stopping.enabled ? 'label' : 'label-disabled'
            }
          >
            Early stopping delta
          </label>
          <input
            className={
              stylingConfig.early_stopping.enabled ? 'input' : 'input-disabled'
            }
            id="early-stopping-delta"
            type="number"
            min="0"
            max="1"
            step="1e-5"
            disabled={!stylingConfig.early_stopping.enabled}
            value={stylingConfig.early_stopping.delta}
            onChange={(event) => setStylingConfig({
              ...stylingConfig,
              early_stopping: {
                ...stylingConfig.early_stopping,
                delta: parseFloat(event.target.value),
              },
            })}
          />
        </div>

        <div className="row">
          <label
            htmlFor="early-stopping-patience"
            className={
              stylingConfig.early_stopping.enabled ? 'label' : 'label-disabled'
            }
          >
            Early stopping patience
          </label>
          <input
            className={
              stylingConfig.early_stopping.enabled ? 'input' : 'input-disabled'
            }
            id="early-stopping-patience"
            type="number"
            min="0"
            max="1e4"
            step="1"
            disabled={!stylingConfig.early_stopping.enabled}
            value={stylingConfig.early_stopping.patience}
            onChange={(event) => setStylingConfig({
              ...stylingConfig,
              early_stopping: {
                ...stylingConfig.early_stopping,
                patience: parseInt(event.target.value, 10),
              },
            })}
          />
        </div>

        <div className="row">
          <label className="label" htmlFor="tags-gatys">
            <div className="tags-sublabel">Tags (optional)</div>
          </label>
          <input
            className="tags-input"
            id="tags-gatys"
            maxLength="20"
            value={tags}
            onChange={(event) => setTags(event.target.value)}
          />
        </div>
      </div>

      <div className="buttons">
        <span className="button-block">
          <Button
            size="lg"
            variant="secondary"
            className="button"
            onClick={() => setStylingConfig(defaultStylingConfig)}
          >
            Reset
          </Button>
        </span>
        {runDisabledReason === 'OK'
          ? (
            <span className="button-block">
              <Button
                size="lg"
                type="submit"
                variant="success"
                className="button"
              >
                Run
              </Button>
            </span>
          )
          : (
            <OverlayTrigger overlay={(<Tooltip>{runDisabledReason}</Tooltip>)}>
              <span className="button-block">
                <Button
                  size="lg"
                  type="submit"
                  variant="success"
                  className="button"
                  disabled
                  style={{ pointerEvents: 'none' }}
                >
                  Run
                </Button>
              </span>
            </OverlayTrigger>
          )}
      </div>

    </form>
  );
}

GatysCard.defaultProps = {
  baseState: null,
  styleState: null,
  synthesizedContainer: null,
};

GatysCard.propTypes = {
  baseState: imageStateProp,
  styleState: imageStateProp,
  synthesizedContainer: imageContainerProp,
  masksValid: PropTypes.bool.isRequired,
};

export default GatysCard;
