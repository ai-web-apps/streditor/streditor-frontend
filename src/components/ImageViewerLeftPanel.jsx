import React, { useState } from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import {
  SizeButton,
  CropButton,
  ColourButton,
  MaskButton,
} from './ImageViewerButtons';
import SizeModal from './modals/SizeModal';
import CropModal from './modals/CropModal';
import ColourModal from './modals/ColourModal';
import MaskModal from './modals/MaskModal';
import { imageTypeProp, imageIndexProp, imageContainerProp } from '../utils/props';
import '../style/image-viewer.scss';

function ImageViewerLeftPanel({ type, index, container }) {
  const [sizeModalIsOpen, setSizeModalIsOpen] = useState(false);
  const [cropModalIsOpen, setCropModalIsOpen] = useState(false);
  const [colourModalIsOpen, setColourModalIsOpen] = useState(false);
  const [maskModalIsOpen, setMaskModalIsOpen] = useState(false);

  return (
    <div className="image-viewer-side-panel">
      <div className="left-side">
        <ButtonGroup vertical aria-label="Tools" className="buttons-group">
          <SizeButton setModalIsOpen={setSizeModalIsOpen} />
          <CropButton setModalIsOpen={setCropModalIsOpen} />
          <ColourButton setModalIsOpen={setColourModalIsOpen} />
          <MaskButton type={type} setModalIsOpen={setMaskModalIsOpen} />
        </ButtonGroup>
        <SizeModal
          isOpen={sizeModalIsOpen}
          setIsOpen={setSizeModalIsOpen}
          type={type}
          index={index}
          container={container}
        />
        <CropModal
          isOpen={cropModalIsOpen}
          setIsOpen={setCropModalIsOpen}
          type={type}
          index={index}
          container={container}
        />
        <ColourModal
          isOpen={colourModalIsOpen}
          setIsOpen={setColourModalIsOpen}
          type={type}
          index={index}
          container={container}
        />
        <MaskModal
          isOpen={maskModalIsOpen}
          setIsOpen={setMaskModalIsOpen}
          type={type}
          index={index}
          container={container}
        />
      </div>
    </div>
  );
}

ImageViewerLeftPanel.propTypes = {
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
  container: imageContainerProp.isRequired,
};

export default ImageViewerLeftPanel;
