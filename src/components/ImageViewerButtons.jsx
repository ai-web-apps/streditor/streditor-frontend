import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import { useDispatch } from 'react-redux';
import { useDropzone } from 'react-dropzone';
import { ImDownload } from 'react-icons/im';
import { MdDeleteForever } from 'react-icons/md';
import { GiResize, GiDualityMask } from 'react-icons/gi';
import { BiReset, BiCrop, BiImageAdd } from 'react-icons/bi';
import { CgArrowLeftR, CgEditContrast } from 'react-icons/cg';
import { getBase64Extension } from '../utils/strings';
import { MAX_FILES, IMAGE_TYPES } from '../utils/constraints';
import { imageTypeProp, imageIndexProp } from '../utils/props';
import { loadExistingImage, uploadNewImage } from '../utils/images';
import { resetState, removeImage, changeIndex } from '../actions/images';
import '../style/image-viewer.scss';

const BUTTON_SIZE = '22';

export function ResetButton({ type, index }) {
  const dispatch = useDispatch();

  return (
    <Button
      variant="secondary"
      size="lg"
      onClick={() => dispatch(resetState(type, index))}
    >
      <BiReset size={BUTTON_SIZE} />
    </Button>
  );
}

export function SwapButton({ image, index, hash }) {
  const dispatch = useDispatch();

  return (
    <Button
      variant="secondary"
      size="lg"
      onClick={() => {
        loadExistingImage('base', index, image, hash, dispatch);
        dispatch(changeIndex('base', index));
      }}
    >
      <CgArrowLeftR size={BUTTON_SIZE} />
    </Button>
  );
}

export function DownloadButton({ imageName, image }) {
  const imageExtension = getBase64Extension(image);
  const imageElement = document.createElement('a');
  imageElement.href = image;
  imageElement.download = `${imageName}.${imageExtension}`;

  return (
    <Button
      variant="secondary"
      size="lg"
      onClick={() => { imageElement.click(); }}
    >
      <ImDownload size={BUTTON_SIZE} />
    </Button>
  );
}

export function UploadButton({ type, numImages }) {
  const dispatch = useDispatch();
  const onDrop = useCallback(
    (files) => {
      for (let index = 0; index < files.length; index += 1) {
        const reader = new FileReader();
        const imageIndex = numImages + index;

        reader.onload = (event) => {
          uploadNewImage(type, imageIndex, event.target.result, dispatch);
          dispatch(changeIndex(type, imageIndex));
        };
        reader.readAsDataURL(files[index]);
      }
    },
    [type, numImages],
  );

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: IMAGE_TYPES,
    maxFiles: MAX_FILES - numImages,
  });

  return (
    <Button {...getRootProps({ variant: 'secondary', size: 'lg' })}>
      <input {...getInputProps()} />
      <BiImageAdd size={BUTTON_SIZE} />
    </Button>
  );
}

export function RemoveButton({ type, index, numImages }) {
  const dispatch = useDispatch();

  return (
    <Button
      variant="secondary"
      size="lg"
      onClick={() => {
        let newIndex = index;
        if (index === 0 && numImages === 1) {
          newIndex = null;
        } else if (index === numImages - 1) {
          newIndex = index - 1;
        }
        dispatch(removeImage(type, index));
        dispatch(changeIndex(type, newIndex));
      }}
    >
      <MdDeleteForever size={BUTTON_SIZE} />
    </Button>
  );
}

export function SizeButton({ setModalIsOpen }) {
  return (
    <Button
      variant="secondary"
      size="lg"
      onClick={() => setModalIsOpen(true)}
    >
      <GiResize size={BUTTON_SIZE} />
    </Button>
  );
}

export function CropButton({ setModalIsOpen }) {
  return (
    <Button
      variant="secondary"
      size="lg"
      onClick={() => setModalIsOpen(true)}
    >
      <BiCrop size={BUTTON_SIZE} />
    </Button>
  );
}

export function ColourButton({ setModalIsOpen }) {
  return (
    <Button
      variant="secondary"
      size="lg"
      onClick={() => setModalIsOpen(true)}
    >
      <CgEditContrast size={BUTTON_SIZE} />
    </Button>
  );
}

export function MaskButton({ type, setModalIsOpen }) {
  return (
    <Button
      variant="secondary"
      size="lg"
      onClick={() => setModalIsOpen(true)}
      disabled={type === 'synthesized'}
    >
      <GiDualityMask size={BUTTON_SIZE} />
    </Button>
  );
}

ResetButton.propTypes = {
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
};

SwapButton.propTypes = {
  image: PropTypes.string.isRequired,
  index: imageIndexProp.isRequired,
  hash: PropTypes.string.isRequired,
};

DownloadButton.propTypes = {
  imageName: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

UploadButton.propTypes = {
  type: imageTypeProp.isRequired,
  numImages: PropTypes.number.isRequired,
};

RemoveButton.propTypes = {
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
  numImages: PropTypes.number.isRequired,
};

SizeButton.propTypes = {
  setModalIsOpen: PropTypes.func.isRequired,
};

CropButton.propTypes = {
  setModalIsOpen: PropTypes.func.isRequired,
};

ColourButton.propTypes = {
  setModalIsOpen: PropTypes.func.isRequired,
};

MaskButton.propTypes = {
  type: imageTypeProp.isRequired,
  setModalIsOpen: PropTypes.func.isRequired,
};
