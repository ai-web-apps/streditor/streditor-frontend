import React from 'react';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';

function App() {
  return (
    <div className="App" id="app">
      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
