import React, { useSelector } from 'react-redux';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import {
  SwapButton,
  ResetButton,
  RemoveButton,
  UploadButton,
  DownloadButton,
} from './ImageViewerButtons';
import { getIndexFromHash } from '../utils/images';
import { imageTypeProp, imageIndexProp, imageContainerProp } from '../utils/props';
import '../style/image-viewer.scss';

function ImageViewerRightPanel({ type, index, container }) {
  const imageState = container[index];
  const numImages = Object.keys(container).length;

  const getCustomButton = () => {
    if (type === 'synthesized') {
      const baseContainer = useSelector((state) => state.baseContainer);
      const baseIndex = getIndexFromHash(baseContainer, imageState.inputStage.hash);
      return (
        <SwapButton
          image={imageState.outputStage.image}
          index={baseIndex}
          hash={imageState.inputStage.hash}
        />
      );
    }
    return <UploadButton type={type} numImages={numImages} />;
  };

  return (
    <div className="image-viewer-side-panel" id={`${type}-${imageState}`}>
      <div className="right-side">
        <ButtonGroup vertical aria-label="Tools" className="buttons-group">
          <ResetButton type={type} index={index} />
          <RemoveButton type={type} index={index} numImages={numImages} />
          {getCustomButton()}
          <DownloadButton
            imageName={imageState.inputStage.hash}
            image={imageState.outputStage.image}
          />
        </ButtonGroup>
      </div>
    </div>
  );
}

ImageViewerRightPanel.propTypes = {
  type: imageTypeProp.isRequired,
  index: imageIndexProp.isRequired,
  container: imageContainerProp.isRequired,
};

export default ImageViewerRightPanel;
