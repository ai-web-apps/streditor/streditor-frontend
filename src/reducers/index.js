import { combineReducers } from 'redux';
import appReducer from './app';
import configReducer from './configs';
import historyReducer from './history';
import { imageReducer, indexReducer } from './images';

const allReducer = combineReducers({
  app: appReducer,
  history: historyReducer,
  gatys: configReducer('gatys'),
  yijun: configReducer('yijun'),
  reinhard: configReducer('reinhard'),
  baseContainer: imageReducer('base'),
  styleContainer: imageReducer('style'),
  synthesizedContainer: imageReducer('synthesized'),
  baseIndex: indexReducer('base'),
  styleIndex: indexReducer('style'),
  synthesizedIndex: indexReducer('synthesized'),
});

export default allReducer;
