const configReducer = (method) => (state = null, action) => {
  if (action.meta !== method) return state;

  switch (action.type) {
    case 'LOAD_STYLING_CONFIG':
      return action.payload;
    default:
      return state;
  }
};

export default configReducer;
