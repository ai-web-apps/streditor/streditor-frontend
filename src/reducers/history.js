import { instanceOf, string } from 'prop-types';

const historyReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_HISTORY_RECORD':
      if (state.some((record) => record.hash === action.payload.hash)) {
        return state;
      }
      return [
        ...state, {
          tags: action.payload.tags,
          datetime: action.payload.datetime,
          hash: action.payload.hash,
          baseState: action.payload.baseState,
          styleState: action.payload.styleState,
          synthesizedImage: action.payload.synthesizedImage,
          stylingConfig: action.payload.stylingConfig,
        },
      ];
    case 'REMOVE_HISTORY_RECORD':
      return state.filter((record) => record.hash !== action.payload.hash);
    case 'EDIT_HISTORY_RECORD':
      if (!state.some((record) => record.hash === action.payload.hash)) {
        return state;
      }
      return [
        ...state.filter((record) => record.hash !== action.payload.hash),
        action.payload.editedRecord,
      ];
    case 'LOAD_HISTORY': {
      const history = action.payload.slice();
      for (let i = 0; i < history.length; i += 1) {
        if (instanceOf(history[i].datetime, string)) {
          history[i].datetime = new Date(history[i].datetime);
        }
      }
      return history;
    }
    default:
      return state;
  }
};

export default historyReducer;
