export const imageReducer = (imageType) => function reducerSelector(state = {}, action) {
  if (action.meta === undefined) return state; // no current image

  const { type, index } = action.meta;
  const { payload } = action;

  if (type !== imageType) return state; // image of different type

  switch (action.type) {
    case 'CONTAINER': {
      return payload;
    }
    case 'STATE':
      return {
        ...state,
        [index]: payload,
      };
    case 'RESET':
      return {
        ...state,
        [index]: {
          ...state[index],
          sizeStage: {
            ...payload.sizeStage,
            image: state[index].inputStage.image,
            mask: state[index].inputStage.mask,
            width: state[index].inputStage.width,
            height: state[index].inputStage.height,
          },
          cropStage: {
            ...payload.cropStage,
            image: state[index].inputStage.image,
            mask: state[index].inputStage.mask,
          },
          colourStage: {
            ...payload.colourStage,
            image: state[index].inputStage.image,
            mask: state[index].inputStage.mask,
          },
          outputStage: {
            ...payload.outputStage,
            image: state[index].inputStage.image,
            mask: state[index].inputStage.mask,
          },
        },
      };
    case 'MASK':
      return {
        ...state,
        [index]: {
          ...state[index],
          inputStage: payload.inputStage,
          sizeStage: payload.sizeStage,
          cropStage: payload.cropStage,
          colourStage: payload.colourStage,
          outputStage: payload.outputStage,
        },
      };
    case 'UNMASK':
      return {
        ...state,
        [index]: {
          ...state[index],
          inputStage: { ...state[index].inputStage, mask: null },
          sizeStage: { ...state[index].sizeStage, mask: null },
          cropStage: { ...state[index].cropStage, mask: null },
          colourStage: { ...state[index].colourStage, mask: null },
          outputStage: { ...state[index].outputStage, mask: null },
        },
      };
    case 'INPUT':
      return {
        ...state,
        [index]: {
          ...state[index],
          inputStage: payload.inputStage,
          sizeStage: payload.sizeStage,
          cropStage: payload.cropStage,
          colourStage: payload.colourStage,
          outputStage: payload.outputStage,
        },
      };
    case 'SIZE':
      return {
        ...state,
        [index]: {
          ...state[index],
          sizeStage: payload.sizeStage,
          cropStage: payload.cropStage,
          colourStage: payload.colourStage,
          outputStage: payload.outputStage,
        },
      };
    case 'CROP':
      return {
        ...state,
        [index]: {
          ...state[index],
          cropStage: payload.cropStage,
          colourStage: payload.colourStage,
          outputStage: payload.outputStage,
        },
      };
    case 'COLOUR':
      return {
        ...state,
        [index]: {
          ...state[index],
          colourStage: payload.colourStage,
          outputStage: payload.outputStage,
        },
      };
    case 'OUTPUT':
      return {
        ...state,
        [index]: {
          ...state[index],
          outputStage: payload.outputStage,
        },
      };
    case 'REMOVE': {
      const newState = { ...state };
      const lastIndex = Object.keys(state).length - 1;
      for (let i = index; i < lastIndex; i += 1) {
        newState[i] = newState[i + 1];
      }
      delete newState[lastIndex];
      return newState;
    }
    default:
      return state;
  }
};

export const indexReducer = (imageType) => (state = null, action) => {
  if (action.meta === undefined) return state; // no current index

  const { type, index } = action.meta;

  if (type !== imageType) return state; // index of different type

  switch (action.type) {
    case 'INDEX':
      return index;
    default:
      return state;
  }
};
