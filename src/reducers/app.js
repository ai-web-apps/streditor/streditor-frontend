const appReducer = (state = 'idle', action) => {
  switch (action.type) {
    case 'APP_IDLE':
      return 'idle';
    case 'APP_PROCESSING':
      return 'processing';
    default:
      return state;
  }
};

export default appReducer;
