import React from 'react';
import { render, screen } from '@testing-library/react';
import './setupTests';
import Footer from '../components/Footer';

test('app name appears on the page', () => {
  render(<Footer />);
  const linkElement = screen.getByText(/All rights reserved/i);
  expect(linkElement).toBeInTheDocument();
});
